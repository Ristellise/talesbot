import discord
import datetime
import asyncio

activeembeds = []
timeoutseconds = 60


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def getlatestmsgtime(message):
    if message.edited_at is None:
        return message.created_at
    else:
        return message.edited_at


def reactdefaults():
    return ["◀", "⬅", "❎", "➡", "▶"]


class embedpages:
    def __init__(self, listitems, title, description=None, footer=None, inline=True, emojis=None, intochunks=25,
                 reactfootertopages=False):
        """
        :type footer: str
        :type description: str
        :type title: str
        :type listitems: list
        :param listitems: [[Title,value],[Title2,value2]]
        :param title: Embed title
        :param description: Embed Description
        :param footer: embed footer
        :param emojis: [first page,previous page, close, next page,last page]
        """
        if emojis is None:
            emojis = ["◀", "⬅", "❎", "➡", "▶"]
        if len(emojis) != 5:
            print("Wrong number of emojis!")
            emojis = ["◀", "⬅", "❎", "➡", "▶"]
        self.emojis = emojis
        self.inline = inline
        self._msg = None
        self.title = title
        self.desc = description
        self.footer = footer
        self.embeditems = listitems
        self.page = 0
        self.chunks = intochunks
        self.chunked = None
        self.embedpaged = None
        self.currentchunk = None
        self.maxpages = None
        self._author = None
        self.footerinteractive = reactfootertopages
        self.createpages()
        self.setchunk()

    def createpages(self):
        self.chunked = list(chunks(self.embeditems, self.chunks))
        self.maxpages = len(self.chunked) - 1

    def setchunk(self):
        self.currentchunk = self.chunked[self.page]

    def setmsg(self, msg, author):
        self._msg = msg
        self._author = author
        self.addtoembed()

    def getembed(self):
        pageembed = discord.Embed(title=self.title, description=self.desc, color=discord.Color.dark_green())
        if self.footer is not None:
            pageembed.set_footer(text=self.footer)
        elif self.footerinteractive:
            pageembed.set_footer(text=f"{self.page}/{self.maxpages}")
        for x in self.currentchunk:
            pageembed.add_field(name=x[0], value=x[1], inline=self.inline)
        return pageembed

    def addtoembed(self):
        activeembeds.append([self, self._msg, self._author])

    @staticmethod
    def checktimeouts():
        now = datetime.datetime.utcnow()
        timedout = discord.Embed(title="Closed due to inactivity", color=discord.Color.dark_red())
        dotimeout = []

        for x in activeembeds:
            if (now - getlatestmsgtime(x[1])).total_seconds() >= timeoutseconds:
                dotimeout.append(x[1])
                del activeembeds[activeembeds.index(x)]
        dotimeout.append(timedout)
        return dotimeout

    @staticmethod
    def check(msgid, reaction, user, timeout=False):
        for x in activeembeds:
            if str(x[1].id) == str(msgid):
                if str(x[2].id) == str(user.id):
                    pass
                else:
                    return x[0].getembed()
                if timeout:
                    del x
                    return True
                if reaction == x[0].emojis[0]:
                    x[0].firstpage()
                elif reaction == x[0].emojis[1]:
                    x[0].prevpage()
                elif reaction == x[0].emojis[2]:
                    del x
                    return True
                elif reaction == x[0].emojis[3]:
                    x[0].nextpage()
                elif reaction == x[0].emojis[4]:
                    x[0].lastpage()
                return x[0].getembed()
        return False

    def nextpage(self):
        if self.page >= self.maxpages:
            self.page = self.maxpages
        else:
            self.page += 1
            self.setchunk()

    def prevpage(self):
        if self.page == 0:
            pass
        else:
            self.page -= 1
            self.setchunk()

    def firstpage(self):
        self.page = 0
        self.setchunk()

    def lastpage(self):
        self.page = self.maxpages
        self.setchunk()

    async def messageinitalize(self, ctx):
        msg = await ctx.send(embed=self.getembed())
        self.setmsg(msg, ctx.author)
        for x in self.emojis:
            await msg.add_reaction(x)


# Embed Pages Extended Version.
class embedpagesdescriptions(embedpages):
    def __init__(self, listitems, title):
        super().__init__(listitems, title, intochunks=1)

    def getembed(self):
        # Changed this function.
        pageembed = discord.Embed(title=self.title, description=self.desc, color=discord.Color.blue())
        if self.footer is not None:
            pageembed.set_footer(text=self.footer)
        elif self.footerinteractive:
            pageembed.set_footer(text=f"{self.page}/{self.maxpages}")
        pageembed.title = self.currentchunk[0][0][0]
        if self.currentchunk[0][0][1]:
            pageembed.url = self.currentchunk[0][0][1]
        pageembed.description = self.currentchunk[0][1]
        pageembed.set_footer(text=self.currentchunk[0][2])
        if len(self.currentchunk[0]) >= 4:
            pageembed.set_thumbnail(url=self.currentchunk[0][3])
        return pageembed
