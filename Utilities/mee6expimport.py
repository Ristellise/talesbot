import requests

sid = 276035355369799680
page = 0
finaldictionary = {'players': {}}
print(f"Importing... Server ID: {sid}")
while True:
    print(f"Requesting page: {page}")
    a = requests.get(f"https://api.mee6.xyz/plugins/levels/leaderboard/{sid}?page={page}")
    try:
        a.json()
    except Exception as e:
        break
    if not a.json()['players']:
        break
    else:
        for x in a.json()['players']:
            finaldictionary['players'][str(x['id'])] = x['xp']
        page += 1
print(finaldictionary)
