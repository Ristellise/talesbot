import json
import os
import time
import textwrap
import typing
from fuzzywuzzy import fuzz, process
import discord
import struct, bitarray

guilds = []
"""
Guild Json Utilities. 
"""


class sj:
    def __init__(self, server):
        self.server = server
        self.sj = {}
        self.path = os.path.join(os.getcwd(), 'guilds', f"{str(self.server)}.json")

    def readjson(self):
        if os.path.exists(self.path):
            with open(self.path, "r", encoding="utf-8") as f:
                try:
                    self.sj = json.loads(f.read())
                    return 0, "OK"
                except json.JSONDecodeError:
                    return 500, "Json Failed to load file."
        else:
            self.sj = {}
            with open(self.path, "w", encoding="utf-8") as f:
                f.write(json.dumps(self.sj))
                return 200, "Created Blank Server. Continue."

    def writejson(self):
        with open(self.path, "w", encoding="utf-8") as f:
            f.write(json.dumps(self.sj, indent=4))
            return 0, "OK"


class JsonGuild:
    def __init__(self, guild):
        self.guildid = str(guild)
        self.guildfile = sj(self.guildid)
        self.guildfile.readjson()
        self.lastaccessed = int(time.time())
        self.lastmodified = self.get_modifiedtime()

    def get_modifiedtime(self):
        return int(os.stat(self.guildfile.path).st_mtime)

    def initdefaultsandwrite(self):
        self.guildfile.sj.setdefault("quote", {})
        self.guildfile.sj.setdefault("emote", {})
        self.guildfile.sj.setdefault("tracking", {})
        self.guildfile.sj.setdefault("config", {})
        self.guildfile.sj.setdefault("fid", {})
        self.writejson()

    # --- Quotes --- #
    def quote_add(self, tag, quote):
        self.guildfile.sj.setdefault("quote", {})
        attempt = self.guildfile.sj["quote"].setdefault(tag, quote)
        if attempt != quote:
            counter = 1
            while True:
                attempt = self.guildfile.sj["quote"].setdefault(f"{tag}{counter}", quote)
                if attempt != quote:
                    counter += 1
                else:
                    break
        self.writejson()
        return 0, (f"{quote}", tag)

    def quote_get(self, tag):
        self.refresh_json()
        self.guildfile.sj.setdefault("quote", {})
        q = self.guildfile.sj["quote"].get(tag, None)
        if q is None:
            return [404, f"Can't find tag:\"{tag}\". Since it doesn't exist."]
        else:
            return [0, q]

    def quote_remove(self, tag):
        self.guildfile.sj.setdefault("quote", {})
        if self.guildfile.sj["quote"].get(tag, None) is None:
            return [404, f"Can't remove tag:\"{tag}\". Since it doesn't exist."]
        else:
            self.guildfile.sj["quote"].pop(tag)
            self.writejson()
            return 0, f"Removed tag:\"{tag}\"."

    def quote_list(self):
        self.guildfile.sj.setdefault("quote", {})
        return [[k, textwrap.shorten(v, 100, placeholder="...")] for k, v in self.guildfile.sj['quote'].items()]

    def quote_search(self, string_search):
        self.guildfile.sj.setdefault("quote", {})
        res = []
        _, quotes = map(list, zip(*self.guildfile.sj['quote'].items()))
        for a in process.extract(string_search, quotes, limit=200, scorer=fuzz.token_set_ratio):
            if a[1] > 60:
                res.append(a)
        res = set(res)
        rres = []
        for a in res:
            for i in set(self.guildfile.sj['quote'].items()):
                if a[0] == i[1]:
                    twrap = textwrap.shorten(i[1], 128, placeholder='...\"')
                    rres.append([f"{i[0]} | Match: {a[1]}%", f"{twrap}"])
        if not rres:
            return 404, f"Cannot find any search result matching: \"{string_search}\""
        else:
            return 0, rres

    # --- Emotes --- #
    def emote_add(self, title: str, tags: str, url: str, footer: typing.Optional[str] = ""):
        self.refresh_json()
        self.guildfile.sj.setdefault("emote", [])
        jsontags = []
        for i in self.guildfile.sj["emote"].values():
            jsontags.append(i[2].lower())
        if tags.lower() in jsontags:
            return 404, f"Tag: \"{tags}\" Already exists in the database."
        else:
            self.guildfile.sj['emote'].append([title, url, tags, footer])
            self.writejson()
            return 0, "OK"

    def emote_remove(self, tags: str):
        for i in self.guildfile.sj["emote"].values():
            if i[2].lower() == tags.lower():
                self.guildfile.sj["emote"].pop(i)
                self.writejson()
                return 0, f"Removed Emote with tags: {i[0]}"
        return 404, f"Can't find {tags}."

    def emote_get(self, tags):
        self.refresh_json()
        for i in self.guildfile.sj["emote"].values():
            if i[2].lower() == tags.lower():
                return 0, i
        return 404, "can't find emote with \"{tags}\""

    def emote_list(self):
        self.guildfile.sj.setdefault("emote", [])
        return [[i[0], i[1]] for i in self.guildfile.sj["emote"].values()]

    def refresh_json(self):
        if self.lastmodified != int(os.stat(self.guildfile.path).st_mtime):
            print("File modified. reading again...")
            return self.guildfile.readjson()

    # --- Configs --- #
    def config_wipe(self):
        """
        Wipes the ENITIRE config
        :return:
        """
        self.guildfile.sj['config'] = {}
        self.writejson()

    def get_config(self, configval):
        return self.guildfile.sj.get(configval, None)

    def config(self):
        return self.guildfile.sj.get('config')

    def set_config(self, **kwargs):
        self.refresh_json()
        # PyCharm Messing with Conda's Types...
        # noinspection PyTypeChecker
        for k, v in kwargs.items():
            cfg = self.guildfile.sj.get("config", {})
            cfg[k] = v
            self.guildfile.sj['config'] = cfg
        self.writejson()

    def writejson(self):
        self.guildfile.writejson()
        self.lastmodified = get_modifiedtime()

    def set_tracked_user(self, userid, **kwargs):
        tracking = self.guildfile.sj.setdefault("tracking", {})
        if userid == 0:  # aka global tracking
            tracking.setdefault("global", {})
            tracking["global"]['ban'] = kwargs.get("ban", False)
            tracking["global"]['kick'] = kwargs.get("kick", False)
            tracking["global"]['join'] = kwargs.get("join", False)
            tracking["global"]['leave'] = kwargs.get("leave", False)
            tracking["global"]['logchannel'] = kwargs.get("chan", False)
        else:
            userid = f"{str(userid)}"
            tracking.setdefault(userid, {})
            tracking[userid]['edited'] = kwargs.get("edit", False)
            tracking[userid]['created'] = kwargs.get("create", False)
            tracking[userid]['deleted'] = kwargs.get("delete", False)

    def get_tracking(self, userid):
        return self.guildfile.sj.get(str(userid), {})

    def get_friendid_dict(self):
        return self.guildfile.sj.setdefault("fid", {})

    def set_friendid(self, discordid, name, followid, message):
        currentdict = self.get_friendid_dict()
        currentdict.setdefault(f"{discordid}", {})
        currentdict[discordid][name] = [followid, message]
        self.writejson()

    def get_friendid(self, discordid):
        currentdict = self.get_friendid_dict()
        return currentdict.get(discordid, {})


class FriendID:

    def __init__(self, name, followid, message=""):
        self.followid = followid
        self.message = message

    def pack(self):
        return [self.name, self.followid, self.name]

    @classmethod
    def unpack(cls, packedlist):
        return cls(packedlist[0], packedlist[1], packedlist[2])


def timeout():
    tnow = int(time.time())
    for i in guilds:
        if tnow > i.lastaccessed + 3600:
            guilds.remove(i)


def get_instance(guild):
    if isinstance(guild, discord.Guild):
        guild = guild.id
    for i in guilds:
        if i.guildid == str(guild):
            return i
    g = JsonGuild(guild)
    guilds.append(g)
    return g
