#!/usr/bin/env python
# -*- coding: utf-8 -*-
import difflib
import itertools
import json
import logging
import re
import traceback
import unicodedata

import aiohttp
import discord
import hurry.filesize
import validators
from discord.ext.commands.cooldowns import Cooldown, CooldownMapping, BucketType

import Dconst
from Utilities import serverroutines

# Headers as some sites are annoying pepole :)
headers = {'User-Agent': f'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) '
f'Chrome/50.0.2661.102 Safari/537.36 JWTalesBotV2/{Dconst.version[0]}'}
_digits = re.compile('\d')
logger = logging.getLogger("Utilities")


def grouper(n, iterable, fillvalue=None):
    """grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"""
    args = [iter(iterable)] * n
    return itertools.zip_longest(fillvalue=fillvalue, *args)


def strmatchwildcase(slist: list, message: str):
    """
    same as strmatchwild. But This ignores cases.
    :param slist: list of strings to check
    :param message: string
    :return: bool
    """
    for words in slist:
        message = message.lower()
        if message.find(words) == -1:
            pass
        else:
            return True
    return False


def printstack():
    stack = traceback.format_stack()
    for i in stack:
        logger.warning(i)


def contains_digits(d):
    """
    does string haz digits?
    :param d: message
    :return: True or False
    """
    return bool(_digits.search(d))


def rgb2int(ctuple: tuple):
    """

    :param ctuple: tuple of colours (R, G, B)
    :return: RGB in int form
    """
    r = ctuple[0]
    g = ctuple[1]
    b = ctuple[2]
    cint = (r << 16) + (g << 8) + b
    return cint


def findWholeWord(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search


# Coroutine pls.
async def trydel(ctx, quiet=True):
    if serverroutines.Config.get(ctx.guild, 'do-not-delete-messages') in ['y', 'yes', 'true', True, '1']:
        pass
    else:
        # Discord guys said that i should do ctx.message.delete() :P
        try:
            await ctx.message.delete()
        except discord.Forbidden:
            if quiet is False:
                await ctx.send("Unable to Delete Message.")
        except AttributeError as e:
            if 'Message' in e and 'message' in e:
                logger.warning("Called trydel, but ctx is a Message object!")
                printstack()
                await trydelmsg(ctx, quiet)
            print("Attribute Error! Please Check code. {}".format(e))
        except discord.NotFound:
            logger.info("trydel's ctx is invalid!")


async def trydelmsg(msg, quiet=True):
    if serverroutines.Config.get(msg.guild, 'do-not-delete-messages') in ['y', 'yes', 'true', True, '1']:
        pass
    else:
        # Discord guys said that i should do ctx.message.delete() :P
        try:
            await msg.delete()
        except discord.Forbidden:
            if quiet is False:
                await msg.send("Unable to Delete Message.")
        except AttributeError as e:
            print("Attribute Error! Please Check code. {}".format(e))
        except aiohttp.ClientOSError:
            logger.warning("Aiohttp ClientOSError recieved. Discardning silently.")
            return
        except Exception as e:
            logger.warning("Exception from trydelmsg")
            logger.warning(e)
            printstack()


def serverinfo():
    with Dconst.process.oneshot():
        return [Dconst.process.cpu_percent(), hurry.filesize.size(Dconst.process.memory_info().rss)]


async def asyncrequest(asynctype, url, dojson=False, data=None, headers=None):
    """
    :param headers: clientsession headers
    :param asynctype: GET,POST,PUT etc...
    :param url:
    :param dojson:
    :return: json or the request object.
    :param data: data dict... or something...
    """
    async with aiohttp.ClientSession(headers={'User-Agent': f"Yuribot ({Dconst.mail}, {Dconst.version[0]})"}) as cs:
        async with cs.request(method=asynctype, url=url, headers=headers, data=data) as resp:
            await resp.read()
            response = resp
    if dojson:
        try:
            return await response.json()
        except aiohttp.ClientResponseError:
            return json.loads(await response.read())
    else:
        return response


async def serverargs(_, message):
    try:
        res = serverroutines.Config.get(message.guild, 'Prefix')
    except AttributeError:
        return Dconst.prefix
    if res == ['']:
        return Dconst.prefix
    elif res is None:
        return Dconst.prefix
    else:
        if isinstance(serverroutines.Config.get(message.guild, 'Prefix'), int):
            return [''] + Dconst.prefix
        else:
            return serverroutines.Config.get(message.guild, 'Prefix') + Dconst.prefix


class weeb:
    """
    weeb.sh constructor
    proper usage:
    Utils.weeb(False,imgtype='hug').weeburl()
    """

    def __init__(self, nsfw, imgtype=None, tags=None):
        self.nsfw = nsfw
        self.imgtype = imgtype
        self.tags = tags

    @staticmethod
    def weeb_constructor(nsfw, imgtype=None, tags=None):
        weebstring = "?"
        if imgtype is not None:
            weebstring += f"type={imgtype}"
        if tags is not None:
            weebstring += f"&tags={tags}"
        if nsfw is False:
            weebstring += "&nsfw=false"
        else:
            weebstring += "&nsfw=true"
        return weebstring

    def weeburl(self):
        string = self.weeb_constructor(nsfw=self.nsfw, imgtype=self.imgtype, tags=self.tags)
        return f"https://api.weeb.sh/images/random{string}"


def validate_url(url):
    if not validators.url(url, public=True):
        return False
    else:
        return True


def similar(a, b):
    return difflib.SequenceMatcher(None, a, b).ratio()


def listexists(alist: list, index):
    if len(alist) >= index:
        return True
    else:
        return False


# This is for the gacha command
bypassablecooldown = Cooldown(10, 3600, BucketType.user)
bypassablecooldownmapping = CooldownMapping(bypassablecooldown)


def bpyassablecooldown(ctx):
    if ctx.author.id == 175971332784259072:
        return True
    if bypassablecooldownmapping.get_bucket(ctx).get_tokens() == 0:
        return False
    else:
        return True


def replacelists(text: str, argspair: list):
    for i in argspair:
        text = text.replace(i[0], i[1])
    return text


def decantify(text):
    return unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore').decode().strip()


def decantifyplus(text):
    text = replacelists(text,
                        [["☿", "Y"], ["☋", "U"], ["♫", "n"], ["❡", "g"], ["◗", "D"], ["ꍏ", "A"], ["☈", "R"], ["€", "E"],
                         ["♗", "i"], ["☾", "C"], ["ϰ", "k"], ["¶", "p"], ["π", "tt"], ["ω", "w"], ["Σ", "E"],
                         ["ん", "n"],
                         ["¥", "Y"], ["⊙", "o"]])
    return decantify(text)


def asciify(msg):
    return msg.encode('ascii', errors='ignore').decode('ascii')


from string import Formatter


def strfdelta(tdelta, fmt):
    f = Formatter()
    d = {}
    l = {'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
    k = map(lambda x: x[1], list(f.parse(fmt)))
    rem = int(tdelta.total_seconds())

    for i in ('D', 'H', 'M', 'S'):
        if i in k and i in l.keys():
            d[i], rem = divmod(rem, l[i])

    return f.format(fmt, **d)
