#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import random
import time
import textwrap
from calendar import calendar

import discord
import copy
import Dconst
from Utilities import Utils
from .serverjsons import *

"""
Serverroutines:
High Level: Add, Remove and get for quotes and emotes.
Error Codes as follows:
0: No Errors! It passed correctly.
1: No errors. Only a slight warning about an issue
2: you messed up.
"""


class ServerUtils:

    @staticmethod
    def correct_serverid(server):
        if isinstance(server, int):
            return server
        else:
            return server.id


# This is the higer level of quotes. interface with this instead
# of directly at the low level.
class Quotes:
    @staticmethod
    def add(addquote: str, tag, server=None):
        talestag = tag
        sj = serverjson(server)
        sj.readjson()
        try:
            if sj.serverjson['quote'].get(talestag, 0) != 0:
                trycount = 1
                while True:
                    talestag = f'{tag}{str(trycount)}'
                    logging.info(talestag)
                    if sj.serverjson['quote'].get(talestag, 0) == 0:
                        sj.serverjson['quote'][talestag] = addquote
                        break
                    else:
                        trycount += 1
            else:
                sj.serverjson['quote'][talestag] = addquote
        except KeyError:
            sj.serverjson['quote'] = {}
            if sj.serverjson['quote'].get(talestag, 0) != 0:
                trycount = 1
                while True:
                    talestag = f'{tag}{str(trycount)}'
                    logging.info(talestag)
                    if sj.serverjson['quote'].get(talestag, 0) == 0:
                        sj.serverjson['quote'][talestag] = addquote
                        break
                    else:
                        trycount += 1
            else:
                sj.serverjson['quote'][talestag] = addquote
        sj.writejson()
        return [0, talestag, addquote]

    @staticmethod
    def remove(tagsstring: str, server=None):
        qclass = serverjson(server)
        qclass.readjson()
        try:
            qclass.serverjson['quote'].pop(tagsstring)
            qclass.writejson()
            return [0, f"Removed quote tag: {tagsstring}"]
        except KeyError:
            return [2, f"unable to remove quote number: {tagsstring}"]

    @staticmethod
    def get(uniquetag=None, server=None):
        """
        attempts to get a quote.
        :param uniquetag:
        :param server: Param for server. depends on the server it was sent
        :return: (error code),(string)
        """
        sj = serverjson(server)
        sj.readjson()
        if not isinstance(uniquetag, str):
            a, quote = random.choice(list(sj.serverjson['quote'].items()))
            return [0, quote, a]
        else:
            if sj.serverjson['quote'].get(uniquetag, 0) == 0:
                return [2, f"No Item with Tags: {uniquetag}", uniquetag]
            else:
                return [0, sj.serverjson['quote'].get(uniquetag, 0), uniquetag]

    @staticmethod
    def list(server):
        sj = serverjson(server)
        sj.readjson()
        returnlist = []
        for x in list(sj.serverjson['quote'].keys()):
            returnlist.append([x, textwrap.shorten(sj.serverjson['quote'].get(x), 100, placeholder="...")])
        return returnlist

class Emotes:
    @staticmethod
    def empty(server):
        jsonobject = serverjson(server)
        jsonobject.readjson()
        if jsonobject.serverjson.get('emote', None) is None:
            jsonobject.serverjson['emote'] = []
            jsonobject.writejson()

    @staticmethod
    def add(items: list, server=None):
        Emotes.empty(server)
        sj = serverjson(server)
        sj.readjson()
        elist = sj.serverjson.get('emote', [])
        for emote in elist:
            if emote[2] == items[2]:
                embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('warn'))
                embed.title = "There is a exact tag in the database!"
                return embed
        sj.serverjson['emote'].append(items)
        sj.writejson()
        embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
        embed.title = items[0]
        embed.set_image(url=items[1])
        embed.set_footer(text=items[3])
        embed.add_field(name="Tag:", value=items[2])
        return embed

    @staticmethod
    def get(server, tag: str):
        Emotes.empty(server)
        sj = serverjson(server)
        sj.readjson()
        elist = sj.serverjson.get('emote')
        tag = tag.lower()
        for emote in elist:
            if emote[2] == tag:
                embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
                embed.title = emote[0]
                embed.set_image(url=emote[1])
                embed.add_field(name="Tag:", value=emote[2])
                embed.set_footer(text=emote[3]) if Utils.listexists(emote, 3) else None
                return embed
        embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('warn'))
        embed.title = f"Can't find: {tag}"
        return embed

    @staticmethod
    def list(server):
        sj = serverjson(server)
        sj.readjson()
        elist = sj.serverjson.get('emote')
        relist = []
        for i in elist:
            relist.append([i[0], i[2]])
        return relist


class Config:
    @staticmethod
    def get(server, value):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        try:
            # logging.info(sj.serverjson['config'][value])
            return sj.serverjson['config'][value]
        except KeyError:
            sj.serverjson['config'] = {}
            return 0
        except Exception:
            if value == 'volume':
                sj.serverjson['config'][value] = 0.5
            elif value == 'skipsreq':
                sj.serverjson['config'][value] = 3
            elif value == 'userairia':
                sj.serverjson['config'][value] = 0
            else:
                return 0
            if sj.serverjson['config'][value] in ['true']:
                return True
            # logging.info(sj.serverjson['config'][value])
            return sj.serverjson['config'][value]

    @staticmethod
    def set(server, key, value):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        try:
            sj.serverjson['config'][key] = value
        except Exception:
            sj.serverjson['config'] = {}
            sj.serverjson['config'][key] = value
        sj.writejson()
        return True

    @staticmethod
    def createroles(serverjsonobject):
        try:
            serverjsonobject.serverjson['config']['role']
        except Exception:
            serverjsonobject.serverjson['config']['role'] = {}
            serverjsonobject.writejson()
        return True

    @staticmethod
    def roleset(server, roleid, item, value):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        Config.createroles(sj)
        try:
            sj.serverjson['config']['role'][roleid][item] = value
        except Exception:
            sj.serverjson['config']['role'][roleid] = {}
            sj.serverjson['config']['role'][roleid][item] = value
        sj.writejson()

    @staticmethod
    def roleget(server, roleid):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        Config.createroles(sj)
        return sj.serverjson['config']['role'].get(roleid, None)

    @staticmethod
    def rolelist(server, rolefilter):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        Config.createroles(sj)
        returnroles = []
        for role in sj.serverjson['config']['role'].items():
            roleg = role[1]
            if rolefilter is None:
                returnroles.append(role)
            elif roleg.get(rolefilter, False):
                returnroles.append(role)
        return returnroles


class Friends:
    @staticmethod
    def empty(server):
        sj = serverjson(server)
        sj.readjson()
        sj.serverjson['friends'] = {}
        sj.writejson()

    @staticmethod
    def get(server, discordid):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        try:
            sj.serverjson['friends']
        except KeyError:
            Friends.empty(server)
            return {'JP': [None, None], 'WW': [None, None]}
        try:
            return sj.serverjson['friends'][str(discordid)]
        except KeyError:
            return {'JP': [None, None], 'WW': [None, None]}

    @staticmethod
    def set(server, accversion, discordid, bamcoid, notes=None):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        discordid = str(discordid)
        try:
            sj.serverjson['friends']
        except KeyError:
            Friends.empty(server)
            sj.writejson()
            sj.readjson()
        try:
            frienddict = sj.serverjson['friends'][discordid]
        except KeyError:
            frienddict = sj.serverjson['friends'][discordid] = {}

        frienddict['JP'] = [bamcoid, notes] if accversion == 'JP' else frienddict.get('JP', [None, None])
        if accversion == "WW":
            return True
        sj.writejson()
        return True

    @staticmethod
    def _resolve_dict(list_resolver_dict: dict):
        return "\n".join([f"JP: {list_resolver_dict['JP'][0]}\n"
                          f"{textwrap.shorten(list_resolver_dict.get('JP')[1] if list_resolver_dict.get('JP',[None,None])[1] is not None else 'None', width=30, placeholder='...')}",
                          f"WW: {list_resolver_dict['WW'][0]}\n"
                          f"{textwrap.shorten(list_resolver_dict.get('WW')[1] if list_resolver_dict.get('WW',[None,None])[1] is not None else 'None', width=30, placeholder='...')}"])

    @staticmethod
    def list(server):
        server = ServerUtils.correct_serverid(server)
        sj = serverjson(server)
        sj.readjson()
        try:
            sj.serverjson['friends']
        except KeyError:
            Friends.empty(server)
            sj.writejson()
            sj.readjson()
        return [[ab, Friends._resolve_dict(sj.serverjson['friends'][ab])] for ab in sj.serverjson['friends']]


class UserInfo:
    @staticmethod
    def empty(sj):
        sj.readjson()
        sj.serverjson['users'] = {}
        sj.writejson()

    @staticmethod
    def create(server):
        sj = serverjson(server)
        sj.readjson()
        try:
            sj.serverjson['users']
        except KeyError:
            UserInfo.empty(sj)
            sj.writejson()
            sj.readjson()
        return sj

    @staticmethod
    def existancecreate(sj, userid):
        """
        Spawns in your user
        :param sj: serverjson
        :param userid: discordid
        :return:
        """
        getter = sj.serverjson['users'].get(str(userid), False)
        if not getter:
            sj.serverjson['users'][str(userid)] = {}
            return sj.serverjson['users'][str(userid)]
        else:
            return sj.serverjson['users'][str(userid)]

    @staticmethod
    def update(userid, what, value, server):
        """
            Updates a specific piece of info for the user.
        :param server: discordserver.
        :param userid: discorduserid.
        :param what: what to update?
        :param value: new value [Note: lastmessage can only be gotten, you can't set it.]
        :return:
        """
        sj = UserInfo.create(server)
        userobject = UserInfo.existancecreate(sj, userid)
        if what.lower() == 'gald':
            userobject['gald'] = value
        elif what.lower() == 'exp':
            userobject['exp'] = value
        elif what.lower() == 'friendsid':
            userobject['bindfriend'] = value
        elif what.lower() == 'bio':
            userobject['bio'] = value
        elif what.lower() == 'social':
            userobject['social'] = value
        elif what.lower() == 'lastmessage':
            userobject['lastmessage'] = calendar.timegm(time.gmtime())
        elif what.lower() == 'games':
            userobject['games'] = value
        else:
            logging.fatal(f"I can't Believe this is not: {what.lower()}! | "
                          f"PS: Please fix it. check your code btw.")
            return False
        sj.writejson()
        return True

    @staticmethod
    def get(userid, server, what=None):
        """
        Get's a specific Field
        :param userid:
        :param server:
        :param what:
        :return:
        """
        sj = UserInfo.create(server)
        userobject = UserInfo.existancecreate(sj, userid)
        if what is None:
            return userobject
        else:
            return userobject.get(what.lower(), None)

    @staticmethod
    def formater(userobject: dict, username, profilepic):
        userembed = discord.Embed(title=username)
        for item, value in userobject.items():
            if item in 'games':
                userembed.add_field(name="Games", value="For games, do !profile [User] games")
            else:
                userembed.add_field(name=item, value=value, inline=False)
        userembed.set_thumbnail(url=profilepic)
        return userembed

    class FriendsRemap:
        @staticmethod
        def get(user, server):
            Friends.get(server, user)

        @staticmethod
        def update(bandaiversion, server, discordid, bandaiid, notes):
            Friends.set(server, bandaiversion, bandaiid, discordid, notes)

        @staticmethod
        def bindfid2profile(friendid: int, userprofile: int, server: int):
            UserInfo.update(userprofile, 'bindfriend', friendid, server)

    class Games:
        @staticmethod
        def add(user: int, gamename: str, server: int, additionalinfo: str or None = None):
            currentgames = UserInfo.get(user, server, 'games')
            if currentgames is None:
                currentgames = []
            currentgames.append([gamename.lower(), additionalinfo])
            UserInfo.update(user, 'games', currentgames, server)
            return True

        @staticmethod
        def remove(user: int, gamename: str, server: int):
            currentgames: list = UserInfo.get(user, server, 'games')
            if currentgames == [] or currentgames is None:
                return "Empty Game List"
            else:
                existance = currentgames.index(gamename.lower())
                if existance:
                    currentgames.pop(existance)
                    return True
                else:
                    return "Does not Exist."

        @staticmethod
        def glist(user: int, server: int):
            return UserInfo.get(user, server, 'games')

# TODO: Refactor FIDS
class FIDS:

    def __init__(self, discordserver, ctx, discordid=None, friend_id=None, notes=None):
        """
        Friend ID Library.
        :param friend_id:
        """
        self.fid = friend_id
        self.server = discordserver
        self.duid = discordid
        self.raysver = 'JP'
        self.notes = notes
        self.ctx = ctx

    def add(self):
        if self.fid is None:
            return "Tried to get from a \"set\" object?"
        Friends.set(self.server, self.raysver, self.duid, self.fid, self.notes)
        embed = discord.Embed(title=f"Added! Check your info:")
        addobject = Friends.get(self.server, self.duid)
        embed.add_field(name='<:diamond:425634970464288769> Japan',
                        value="None" if addobject['JP'][0] is None else
                        f"{addobject.get('JP',None)[0]}\n"
                        f"{'' if addobject.get('JP',[None,None])[1] is None else addobject.get('JP',[None,None])[1]}",
                        inline=False)
        embed.add_field(name='<:mirrogems:425634914558410764> Worldwide [Legacy]',
                        value="None" if addobject.get('WW', [None, None])[0] is None else
                        f"ID: {addobject.get('WW',[None,None])[0]}\n"
                        f"{'' if addobject.get('WW',[None,None])[1] is None else addobject.get('WW',[None,None])[1]}",
                        inline=False)
        embed.set_footer(text=f"Requested by: {self.ctx.author}")
        return embed

    def remove(self):
        Friends.set(self.server, self.raysver, self.duid, None, None)
        embed = discord.Embed(title=f"{discord.utils.get(self.server.members,id=self.duid)}, Removed {self.raysver}:")
        addobject = Friends.get(self.server, self.duid)
        embed.add_field(name='<:diamond:425634970464288769> Japan',
                        value="None" if addobject['JP'][0] is None else
                        f"{addobject.get('JP',None)[0]}\n"
                        f"{'' if addobject.get('JP',[None,None])[1] is None else addobject.get('JP',[None,None])[1]}",
                        inline=False)
        embed.add_field(name='<:mirrogems:425634914558410764> Worldwide [Legacy]',
                        value="None" if addobject.get('WW', [None, None])[0] is None else
                        f"ID: {addobject.get('WW',[None,None])[0]}\n"
                        f"{'' if addobject.get('WW',[None,None])[1] is None else addobject.get('WW',[None,None])[1]}",
                        inline=False)
        embed.set_footer(text=f"Requested by: {self.ctx.author}")
        return embed

    def list(self):
        embed = discord.Embed(title=f"{discord.utils.get(self.server.members,id=self.duid)}'s Friend ID's")
        addobject = Friends.get(self.server, self.duid)
        if isinstance(str, type(addobject)):
            print(type(addobject))
            embed.title = addobject
            return embed
        embed.add_field(name='<:diamond:425634970464288769> Japan',
                        value="None" if addobject['JP'][0] is None else
                        f"{addobject.get('JP',None)[0]}\n"
                        f"{'' if addobject.get('JP',[None,None])[1] is None else addobject.get('JP',[None,None])[1]}",
                        inline=False)
        embed.add_field(name='<:mirrogems:425634914558410764> Worldwide [Legacy]',
                        value="None" if addobject.get('WW', [None, None])[0] is None else
                        f"ID: {addobject.get('WW',[None,None])[0]}\n"
                        f"{'' if addobject.get('WW',[None,None])[1] is None else addobject.get('WW',[None,None])[1]}",
                        inline=False)
        embed.set_footer(text=f"Requested by: {self.ctx.author}")
        return embed

    @staticmethod
    def get_all(server):
        return Friends.list(server)

    @staticmethod
    def resolveword(text):
        if text is None:
            return text
        if text.lower() == ['japan', 'jp']:
            text = 'JP'
        elif text.lower() == ['worldwide', 'ww']:
            text = 'WW'
        return text.upper()
