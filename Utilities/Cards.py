import json
import os

# 3,4,5,6,7,8
rates = [0.4, 0.3, 0.15, 0.1, 0.04, 0.01]
Data = {}
gacha = None


class Card:
    def __init__(self, CardStr):
        cstr = CardStr.split("] ")
        self.caption = cstr[1:]
        self.chara = cstr[1].split("-")[0]
        self.stars = cstr[1].split("-")[1]


def loadCardsCache():
    with open("Cards.json", "r") as f:
        cards = json.loads(f.read())
    return cards


cards = loadCardsCache()


def updateFiles():
    with open("gachsa.json", "r") as f:
        gacha = json.loads(f.read())
    for k, v in gacha.items():
        c = []
        print(k, v)
        for i in v:
            for a, url in cards:
                if i.lower() in a.lower():
                    c.append({Card(a), url})
        Data[k] = set(c)
