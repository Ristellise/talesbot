#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Server Jsons.
"""
import json
import os

class serverjson:
    def __init__(self, server):
        self.server = server
        self.serverjson = None

    def readjson(self):
        try:
            with open("serverjson/{}.json".format(self.server), "r", encoding='utf-8') as file:
                self.serverjson = json.load(file)
            file.close()
            return [0, "Success!"]
        except NameError:
            return [2, "No file Exists for server."]
        except FileNotFoundError:
            self.serverjson = {}
            serverjson.writejson(self)
            return [2, "No File Existed. Here. Created one."]

    def writejson(self):
        with open("serverjson/{}.json".format(self.server), 'w', encoding='utf-8') as file:
            json.dump(self.serverjson, file)
        file.close()
