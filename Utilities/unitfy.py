class unitfy:
    """
    Performs Imperial to Metric Calcuations. to flip, set the invert variable.
    WIP!
    """
    @staticmethod
    def mass(mass: float, invert=False):
        if not invert:
            return mass * 2.2046
        else:
            return mass / 2.2046

    @staticmethod
    def distance(distance, invert=False):
        if not invert:
            return distance * 1.852
        else:
            return distance / 1.852
