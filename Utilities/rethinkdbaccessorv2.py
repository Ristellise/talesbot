import discord
import rethinkdb as rt
from Constants import getables
import logging

logger = logging.getLogger('RethinkDBAccessorV2')

ifaces = []


class interface:
    def __init__(self, discordserv):
        self.issue = None
        if conn is not None:
            self.connection = conn
            self.server = discordserv
            self.id = discordserv
            self.rtserverinst = self.create_server()
            self.create_index()
        else:
            self.issue = "Connection Failed. Did you check if the server is accessible?"

    def initalise_user(self, discordid):
        if len(list(self.rtserverinst.get_all(str(discordid), index='discord').run(conn))) == 0:
            self.rtserverinst.insert(
                {'discord': str(discordid), 'exp': 0, 'gald': 0, 'rank': 0, 'timestamp': None}).run(
                self.connection)
            return self.get(discordid)
        else:
            logger.info("user Already Exists.")
            return self.get(discordid)

    def create_server(self):
        if str(self.server) not in rt.table_list().run(self.connection):
            rt.db(rt.db_list().run(self.connection)[1]).table_create(str(self.server)).run(self.connection)
        return rt.table(str(self.server))

    def create_index(self):
        indexlist = self.rtserverinst.index_list().run(self.connection)
        for i in ['discord', 'exp', 'gald', 'rank']:
            if i not in indexlist:
                self.rtserverinst.index_create(i).run(self.connection)

    def list_table(self, indexed_by, order, limit=10):
        if order in ['desc', 0]:
            return self.rtserverinst.order_by(indexed_by, rt.desc(indexed_by),
                                              index=indexed_by).limit(limit).run(self.connection)
        else:
            return self.rtserverinst.order_by(indexed_by, rt.asc(indexed_by),
                                              index=indexed_by).limit(limit).run(self.connection)

    def get(self, discordid):
        """
        Get's the User Instance
        :return:
        """
        try:
            return list(self.rtserverinst.get_all(str(discordid), index='discord').run(self.connection))[0]
        except IndexError:
            return self.initalise_user(discordid)

    def update_batch(self, discordid, kwargs):
        """
        Set a specific Value
        :param kwargs: dictionary arguments
        :param discordid: User's DiscordID
        :return:
        """
        self.rtserverinst.get_all(str(discordid), index="discord").update(kwargs).run(self.connection)

    def update(self, discordid, index, values):
        """
        Set a specific Value
        :param discordid: User's DiscordID
        :param index: Value's name
        :param values: Value
        :return:
        """
        self.rtserverinst.get_all(str(discordid), index="discord").update({index: values}).run(self.connection)

    def gald_give(self, discordid, amount):
        self.update(discordid, 'gald', self.get(str(discordid)).get("gald", 0) + amount)
        return self.get(discordid)

    def gald_take(self, amount, discordid):
        dat = self.get(discordid).get("gald", 0)
        if amount > dat:
            self.update(discordid, 'gald', dat - amount)
        else:
            return f"Not enough: Have: {dat} gald, Deducted: {dat - amount}", 200
        return self.get(discordid), 0

    def exp_give(self, discordid, amount):
        discord_user = self.get(str(discordid))
        if discord_user.get("expoff", False):
            return self.get(discordid)
        else:
            self.update(discordid, 'exp', discord_user.get("exp", 0) + amount)
        return self.get(discordid)

    def getposition(self, discordid):
        ranks = self.rtserverinst.order_by(index=rt.desc('exp')).offsets_of(
            rt.row['discord'] == str(discordid)
        ).run(self.connection)
        userobj = self.get(discordid)
        diffs = gettonextlevel(userobj['rank'], userobj['exp'])
        ranks = list(ranks)[0]

        b = int(self.rtserverinst.count().run(self.connection))
        c = self.get(discordid)
        d = diffs
        return ranks, b, c, d


def gettonextlevel(currentrank, currentexp):
    nextexp = getables.Mee6[currentrank + 1]
    return nextexp, nextexp - currentexp, currentexp


def get_instance(discordserv):
    for i in ifaces:
        if i.id == discordserv:
            return i
    inst = interface(discordserv)
    ifaces.append(inst)
    return inst


try:
    conn = rt.connect()
    connected = True
except rt.ReqlDriverError:
    conn = None
    connected = False
