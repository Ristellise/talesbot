import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import Cooldown, CooldownMapping, BucketType
import random
from Utilities import serverroutines, discordembedpages, Utils, rethinkdbaccessorv2
from Constants import getables
import Dconst
from bisect import bisect_left, bisect_right
import logging
import copy

logger = logging.getLogger('REGC')
"""
REGCW:
Roles, Experience, Gald and Configurations and warnings aka the module does most of the admin stuff.
"""
# 60
cfgtime = 60
bypassablecooldown = Cooldown(1, 300, BucketType.guild)
bypassablecooldownmapping = CooldownMapping(bypassablecooldown)


def ReThinkEnabled(ctx=None):
    return rethinkdbaccessorv2.connected


def bpyassablecooldown(ctx):
    if ctx.author.id == 175971332784259072 and reg.modadmin(ctx.author):
        return True
    if bypassablecooldownmapping.get_bucket(ctx).get_tokens() == 0:
        return False
    else:
        return True


class reg:
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.group(name="roles", aliases=['role'])
    async def rolegroup(self, ctx):
        """
        role group command
        :param ctx:
        :return:
        """
        if ctx.invoked_subcommand is None:
            return await ctx.invoke(self.getrolelists)

    @commands.guild_only()
    @rolegroup.command(name='list')
    async def getrolelists(self, ctx):
        """
        Gets a list of assignable Roles from the server.
        """
        assrroles = serverroutines.Config.rolelist(ctx.guild.id, 'assignable')
        if len(assrroles) == 1:
            plural = "Role"
        else:
            plural = "Roles"
        rolelistembed = discord.Embed(title=f"There are {len(assrroles)} Assignable {plural}:",
                                      color=discord.Color.green())
        if len(assrroles) != 0:
            rolestring = "\n".join([discord.utils.get(ctx.guild.roles, id=int(x[0])).name for x in assrroles])
            rolelistembed.description = f"**{rolestring}**"
        else:
            rolelistembed.description = "Try adding some, they are case sensitive though!"
        await ctx.send(embed=rolelistembed)

    @commands.guild_only()
    @rolegroup.command(name='assign', aliases=['add'])
    async def assign(self, ctx, *, role=None):
        if role is None:
            return await ctx.send("You haven't given a Role for me to assign to!")
        getter = discord.utils.get(ctx.guild.roles, name=role)
        if getter:
            roleinst = serverroutines.Config.roleget(ctx.guild.id, str(getter.id))
            if roleinst is None:
                return await ctx.send("Role isn't assignable!")
            elif roleinst.get('assignable', False):
                if getter in ctx.author.roles:
                    return await ctx.send(embed=discord.Embed(description=f"**{ctx.author.display_name}**: "
                                                                          f"You already have **{role}** role!",
                                                              color=discord.Color.red()))
                await reg.giverole(ctx, getter.id, "Assigned by Bot")
                return await ctx.send(embed=discord.Embed(description=f"**{ctx.author.display_name}**: "
                                                                      f"You now have **{role}** role!",
                                                          color=discord.Color.green()))
            else:
                return await ctx.send(
                    embed=discord.Embed(description=f"**{ctx.author.display_name}**: This Role is not Assignable!",
                                        color=discord.Color.red()))
        else:
            return await ctx.send(
                embed=discord.Embed(description=f"**{ctx.author.display_name}**: This Role Does not exist!",
                                    color=discord.Color.red()))

    @commands.guild_only()
    @rolegroup.command(name='remove', aliases=['take'])
    async def remove(self, ctx, *, role=None):
        if role is None:
            return await ctx.send("You haven't given a Role for me to remove!")
        getter = discord.utils.get(ctx.guild.roles, name=role)
        if getter:
            roleinst = serverroutines.Config.roleget(ctx.guild.id, str(getter.id))
            if roleinst is None:
                return await ctx.send("Role isn't assignable!")
            elif roleinst.get('assignable', False):
                if getter not in ctx.author.roles:
                    return await ctx.send(embed=discord.Embed(description=f"**{ctx.author.display_name}**: "
                                                                          f"Can't take **{role}** away from you!",
                                                              color=discord.Color.red()))
                await reg.removerole(ctx, getter.id, "Removed By bot")
                return await ctx.send(embed=discord.Embed(description=f"**{ctx.author.display_name}**: "
                                                                      f"You no longer have **{role}** role.",
                                                          color=discord.Color.green()))
            else:
                return await ctx.send(
                    embed=discord.Embed(description=f"**{ctx.author.display_name}**: This Role is not Assignable!",
                                        color=discord.Color.red()))
        else:
            return await ctx.send(
                embed=discord.Embed(description=f"**{ctx.author.display_name}**: This Role Does not exist!",
                                    color=discord.Color.red()))

    @commands.command(name="exp-disable")
    @commands.check(ReThinkEnabled)
    async def enableuserlevels(self, ctx):
        """
        Enable/Disable Leveling.
        """
        embed = discord.Embed
        serverinst = rethinkdbaccessorv2.get_instance(ctx.guild.id)
        user_object = serverinst.get(str(ctx.author.id))
        if user_object.get("expoff", False):
            serverinst.update(str(ctx.author.id), "expoff", False)
            embed.title = f"Successfully enabled Exp Leveling for **{ctx.author}**."
        else:
            serverinst.update(str(ctx.author.id), "expoff", False)
            embed.title = f"Successfully disabled Exp Leveling for **{ctx.author}**."
        embed.colour = discord.colour.Colour.green()
        return ctx.send(embed=embed)

    @commands.command(name='rank')
    @commands.check(ReThinkEnabled)
    async def getuserrank(self, ctx, user=None):
        auser = None
        if user is not None:
            if "<@" in user and ">" in user:
                auser = ctx.guild.get_member(user.replace("<@", "").replace("<@!", "").replace(">", ""))
            else:
                auser = ctx.guild.get_member_named(user)
        if auser is None and user is not None:
            return await ctx.send(f"Cannot find: {user}")
        elif user is None:
            aid = ctx.author
        else:
            aid = auser

        userrank = discord.Embed(color=discord.Color.gold())
        serverinst = rethinkdbaccessorv2.get_instance(ctx.guild.id)
        rankings = serverinst.getposition(aid.id)
        userrank.set_author(name=f"{aid.display_name}", icon_url=aid.avatar_url)
        userrank.add_field(name="Rank", value=f"{rankings[0]}/{rankings[1]}")
        userrank.add_field(name="Lvl", value=f"{rankings[2]['rank']}")
        userrank.add_field(name="Next Rank at:", value=rankings[3][0], inline=False)
        userrank.add_field(name="Exp Left:", value=rankings[3][1])
        userrank.add_field(name="Current Exp:", value=rankings[3][2])
        return await ctx.send(embed=userrank)

    @commands.command(name='leaderboards')
    @commands.check(ReThinkEnabled)
    async def leaderboardsexp(self, ctx, rank=None):
        discordinst = rethinkdbaccessorv2.get_instance(ctx.guild.id)

        """
        Get's the Exp Board scores.
        """
        try:
            rank = int(rank)
        except Exception:
            rank = 10
        rankings = discordinst.list_table('exp', 'desc', limit=rank)
        embedify = await constructembed(rankings, ctx)
        embedpages = discordembedpages.embedpages(embedify, f"Leaderboards for the top {rank}",
                                                  reactfootertopages=True, inline=False, intochunks=5)
        msg = await ctx.send(embed=embedpages.getembed())
        embedpages.setmsg(msg, ctx.author)
        for x in discordembedpages.reactdefaults():
            await msg.add_reaction(x)

    # noinspection PyIncorrectDocstring
    @commands.guild_only()
    @commands.command(name='configure', aliases=['config'])
    async def configure(self, ctx, role, action, *, value=None):
        """
        Configure the server settings
        Role: Selected Role [Name/RoleID]
        Available Actions [Use the values in the first square brackets]:
        - Assignable [as] [Sets this role as assignable]
        - RemoveAssignable [ras] [Makes this role no appear on the Assignable list and  is not assignable by any user]
        - Moderators [mod] [Make this Role a moderator Role]
        - RemoveModerators [rmod] [Remove Role from being a moderator]
        - Admin [adm] [Make this Role a Admin Role. Same as Moderator for now.]
        - RemoveAdmin [radm] [Remove Role from being a Admin. Same as Moderator for now.]

        - For the follow actions, set role as 'global'
        - levels (values) [lvl] [assign roles based on levels it can be above 100 up tp 5000!] eg: (10,25,50,75,100)
        - level-roles (values) [lvlr] eg: (Regulars;Friends;Veterans;VIP;Closest Friendos)

        :param value:
        :return:
        """
        perms = (ctx.message.channel.permissions_for(ctx.message.author))
        try:
            if role.lower() != 'global':
                value = value.lower()
            else:
                pass
        except AttributeError:
            pass
        definedroles = serverroutines.Config.rolelist(ctx.guild.id, 'admin')
        isadmin = False
        author = await self.bot.application_info()
        author = author.owner.id
        for urole in ctx.author.roles:
            if str(urole.id) in definedroles:
                userrole = definedroles[str(urole.id)]
                if userrole.get('admin', False):
                    isadmin = True
                    break
                else:
                    isadmin = False
        if serverroutines.Config.get(ctx.guild.id, 'bypass') and \
                (ctx.author.id == author):
            isadmin = True
        if perms.administrator:
            isadmin = True
        if not isadmin:
            return await ctx.send("<:no:345240988291301378>")
        selectedrole = discord.utils.get(ctx.guild.roles, name=role)
        if selectedrole is None and role.lower() != 'global':
            return await ctx.send(f"Cannot find Role named: {role} [It's case sensitive as well!]")
        discordpyrole = discord.utils.get(ctx.guild.roles, name=role)
        if selectedrole is not None and role.lower() != 'global':
            if action.lower() == 'as':
                serverroutines.Config.roleset(ctx.guild.id, str(discordpyrole.id), 'assignable', True)
                return await ctx.send(f"Set Role: `{role}` as Assignable.")
            elif action.lower() == 'ras':
                serverroutines.Config.roleset(ctx.guild.id, str(discordpyrole.id), 'assignable', False)
                return await ctx.send(f"Removed Role: `{role}` as Assignable.")
            elif action.lower() == 'mod':
                serverroutines.Config.roleset(ctx.guild.id, str(discordpyrole.id), 'moderator', True)
                return await ctx.send(f"Set Role: `{role}` as Moderator.")
            elif action.lower() == 'rmod':
                serverroutines.Config.roleset(ctx.guild.id, str(discordpyrole.id), 'moderator', False)
                return await ctx.send(f"Removed Role: `{role}` as Moderator.")
            elif action.lower() == 'adm':
                serverroutines.Config.roleset(ctx.guild.id, str(discordpyrole.id), 'admin', True)
                return await ctx.send(f"Set Role: `{role}` as Admin.")
            elif action.lower() == 'radm':
                serverroutines.Config.roleset(ctx.guild.id, str(discordpyrole.id), 'admin', False)
                return await ctx.send(f"Removed Role: `{role}` as Admin.")
            else:
                return await ctx.send(f"Cannot understand: {action} Did you check it is for global?")
        elif role.lower() == 'global':
            if action.lower() == 'lvl':
                levels = value.split(',')
                levels = [int(level) for level in levels]
                serverroutines.Config.set(ctx.guild.id, 'levels', levels)
            elif action.lower() == 'rols':
                roles = value.split(';')
                roleids = []
                for role in roles:
                    role = role.strip()
                    rolething: discord.Role = discord.utils.get(ctx.guild.roles, name=role)
                    if rolething is None:
                        return await ctx.send(f"|{role}| is an invalid Role!")
                    roleids.append(rolething.id)
                serverroutines.Config.set(ctx.guild.id, 'levels-roles', roleids)
                return await ctx.send(f"Set Level Roles to be: {roles}")
            # Ping on level up and you get a new role
            elif action.lower() == "pr":
                doping = serverroutines.Config.get(ctx.guild.id, 'ping_on_role_up')
                if doping == 0:
                    serverroutines.Config.set(ctx.guild.id, 'ping_on_role_up', True)
                    return await ctx.send("Set Pings on Level up to: True")
                else:
                    serverroutines.Config.set(ctx.guild.id, 'ping_on_role_up', 0)
                    return await ctx.send("Set Pings on Level up to: False")

    @staticmethod
    @commands.bot_has_permissions(manage_role=True)
    async def giverole(ctx: discord.ext.commands.Context, roleid, reason="Level up"):
        role = discord.utils.get(ctx.guild.roles, id=int(roleid))
        if role in ctx.author.roles:
            return
        if ctx.me.top_role <= ctx.author.top_role:
            logger.warning(f"May not be able to apply: \"{role}\" To: {Utils.decantify(str(ctx.author))} due to "
                           f"{ctx.author.top_role} being higher than {ctx.me.top_role}!")
        else:
            try:
                await ctx.author.add_roles(role, reason=reason)
            except discord.Forbidden:
                await logger.error(f"Cannot Add Role to: {ctx.author}. Forbidden.")

    @staticmethod
    async def removerole(ctx: discord.ext.commands.Context, roleid, reason="Level up"):
        role = discord.utils.get(ctx.guild.roles, id=int(roleid))
        if ctx.me.top_role <= ctx.author.top_role:
            logger.warning(f"May not be able to apply: \"{role}\" To: {Utils.decantify(str(ctx.author))} due to "
                           f"{ctx.author.top_role} being higher than {ctx.me.top_role}.\n\tAttempting to Add it anyways.")
        else:
            try:
                await ctx.author.remove_roles(role, reason=reason)
            except discord.Forbidden:
                await logger.warning(f"Cannot Add Role to: {ctx.author}. Forbidden.")

    @commands.command('warning', aliases=['warn'])
    async def _warn(self, ctx, userping, *, Reason):
        adminsatus = await self.modadmin(ctx)
        if not adminsatus:
            return
        did = userping.replace(">", "").replace("<@", "")
        embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('fatal'))
        embed.title = "Warning Issued."
        embed.add_field(name="Warned by:", value=str(ctx.author), inline=False)
        embed.add_field(name="For:", value=Reason, inline=False)
        return await ctx.guild.get_member(int(did)).send(embed=embed)

    @staticmethod
    async def check(message: discord.Message, bot):
        if len(message.content) < 5 or len(message.content.split()) <= 1:
            return
        ctx = await bot.get_context(message)
        await reg.giveexp(ctx)

    @staticmethod
    async def giveexp(ctx: discord.ext.commands.Context):
        # Seed it first [Makes it more random.. hopefully.]
        random.seed()
        if ctx.author == ctx.me or bool(ctx.author.bot):
            return
        discordinst = rethinkdbaccessorv2.get_instance(ctx.guild.id)
        userobject = discordinst.get(ctx.author.id)
        exp = userobject.get('exp', 0) + random.randint(15, 25)
        uts = 0 if userobject['timestamp'] is None else userobject['timestamp']
        ts = ctx.message.created_at.timestamp() - uts
        if ts > cfgtime:
            discordinst.update_batch(ctx.author.id, {'timestamp': int(ctx.message.created_at.timestamp()),
                                                     'exp': exp})
            value = discordinst.get(ctx.author.id)
            await reg.checkrank(ctx, value['exp'], value['rank'])
            return

    @staticmethod
    async def checkrank(ctx: discord.ext.commands.Context, exp, currentrank):
        bisected = find_lt(getables.Mee6, exp)
        rank = 0 if currentrank is None else currentrank
        if bisected < -1:
            return
        elif bisected > rank:
            # We have leveled up or something...
            # Check if we ping on role level up.
            if serverroutines.Config.get(ctx.guild.id, 'ping_on_role_up'):
                levels = serverroutines.Config.get(ctx.guild.id, 'levels')
                if bisected in levels:
                    index(bisected, levels)
                else:
                    return
            if serverroutines.Config.get(ctx.guild.id, 'ping_members'):
                username = ctx.author.mention
            else:
                username = Utils.decantify(ctx.author.display_name)
            levelmsg = random.choice(Dconst.levelup).format(userping=username, level=bisected)
            discordinst = rethinkdbaccessorv2.get_instance(ctx.guild.id)
            discordinst.update(ctx.author.id, 'rank', int(bisected))
            await ctx.send(levelmsg)
        await reg.checklevel(ctx, bisected)

    @staticmethod
    async def checklevel(ctx: discord.ext.commands.Context, currentrank):
        levels = serverroutines.Config.get(ctx.guild.id, 'levels')
        if levels == 0:
            return
        for idx, level in enumerate(levels):
            if currentrank >= level:
                roleid = serverroutines.Config.get(ctx.guild.id, 'levels-roles')[idx]
                await reg.giverole(ctx, roleid)

    @commands.guild_only()
    @commands.command(name="flushuser", hidden=True)
    async def flush_user(self, ctx, user):
        if await reg.modadmin(ctx):
            discordinst = rethinkdbaccessorv2.get_instance(ctx.guild.id)
            discordinst.get(user)
            exp, level = user.get("exp", 0), user.get("rank", 0)
            await reg.checkrank(ctx, exp, level)
        else:
            return await ctx.send("https://i.imgur.com/qx8hSnw.png")

    @staticmethod
    async def modadmin(ctx):
        dMember = ctx.author
        adminrole = serverroutines.Config.rolelist(ctx.guild.id, 'admin')
        modrole = serverroutines.Config.rolelist(ctx.guild.id, 'moderator')
        perms = ctx.author.permissions_in(ctx.channel)
        elevation = False
        author = await ctx.bot.application_info()
        author = author.owner.id
        for urole in dMember.roles:
            if str(urole.id) in adminrole:
                userrole = definedroles[str(urole.id)]
                if userrole.get('admin', False):
                    elevation = True
                    break
                else:
                    elevation = False
            elif str(urole.id) in modrole:
                userrole = definedroles[str(urole.id)]
                if userrole.get('moderator', False):
                    elevation = True
                    break
                else:
                    elevation = False
        if serverroutines.Config.get(ctx.guild.id, 'bypass') and \
                (ctx.author.id == author):
            elevation = True
        if perms.administrator:
            elevation = True
        return elevation

    @staticmethod
    async def reflushcheck(ctx: discord.ext.commands.Context, member):
        discordinst = rethinkdbaccessorv2.get_instance(ctx.guild.id)
        userobject = discordinst.get(ctx.author.id)
        if userobject == 'discord':
            return
        currentrank = userobject['rank']
        levelroleids = serverroutines.Config.get(ctx.guild.id, 'levels-roles')
        levels = serverroutines.Config.get(ctx.guild.id, 'levels')
        ranks = [i for i in levels if currentrank >= i]
        toadd = []
        for rank, _ in enumerate(ranks):
            try:
                toadd.append(discord.utils.get(ctx.guild.roles, id=levelroleids[rank]))
            except IndexError:
                pass
        toadd = set(toadd)
        await member.add_roles(*toadd)


def find_lt(a, x):
    """Find rightmost value less than x"""
    i = bisect_left(a, x)
    if i:
        return i - 1
    else:
        return 0


def index(a, x):
    """Locate the leftmost value exactly equal to x"""
    i = bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    else:
        return None


async def constructembed(userlist, ctx: commands.Context):
    local = []
    for i in userlist:
        member = ctx.guild.get_member(int(i['discord']))
        name = None
        if member is None:
            member = str(await ctx.bot.get_user_info(int(i['discord'])))
            try:
                name = str(member)
            except AttributeError:
                pass
        else:
            name = member.display_name
        if name is None:
            name = i['discord']
        local.append([name, f"EXP: {i['exp']}\nRank: {i['rank']}\nGald Balance:{i['gald']}"])
    return local
