from discord.ext import commands
import discord
from discord.ext import commands
from Utilities import serverroutines, Utils, discordembedpages
import Dconst
import numpy.random
import random
import os
from collections import Counter
import concurrent
import asyncio
from Utilities import serverroutines


class users:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(name="fid", aliases=['friend'])
    async def friend(self, ctx, *, user=None):
        """
        Friend ID commands.
        """
        if ctx.invoked_subcommand is None and user is not None:
            if user.split(" ")[0] == 'add':
                return await ctx.invoke(self.add, fid=user.split(" ")[1], notes=" ".join(user.split(" ")[2:]))
            elif user.split(" ")[0] == 'remove':
                return await ctx.invoke(self.rem)
            elif user.split(" ")[0] == 'get':
                return await ctx.invoke(self.get, userping=user.split(" ")[1])
            elif user.split(" ")[0] == 'list':
                return await ctx.invoke(self.list)
            return await ctx.invoke(self.get, user)
        else:
            return await ctx.invoke(self.get, user)

    @friend.command(name="add")
    async def add(self, ctx, fid:int, *, notes=None):
        if notes == "":
            notes = None
        if fid.isdigit() is False:
            return await ctx.send("A friend ID should not consist of any other characters except numbers."
                                  "[This includes commas]")
        elif len(fid) != 9:
            return await ctx.send("ID should be in total 9 characters long. did you mistyped something?")
        addobject = serverroutines.FIDS(ctx.guild, ctx, ctx.author.id, fid, notes)
        return await ctx.send(embed=addobject.add())

    @friend.command(name="remove", aliases=['rem'])
    async def rem(self, ctx):
        """
        Removes your friend ID
        """
        addobject = serverroutines.FIDS(ctx.guild, ctx, ctx.author.id, None, None)
        return await ctx.send(embed=addobject.remove())

    @friend.command(name="get")
    async def get(self, ctx, userping=None):
        gid = userping.replace("<@", "").replace(">", "") if userping is not None else str(ctx.author.id)
        try:
            int(userping)
        except TypeError:
            gid = str(ctx.author.id)
        except ValueError:
            gid = ctx.message.guild.get_member_named(userping)
            if gid is None:
                gid = -1
            else:
                gid = str(gid.id)
        getobject = serverroutines.FIDS(ctx.guild, ctx, gid)
        return await ctx.send(embed=getobject.list())

    @friend.command(name="list")
    async def list(self, ctx):
        serverlist = serverroutines.FIDS.get_all(ctx.guild.id)
        if not serverlist:
            return await ctx.send("There isnt any Friend ID's in the list!")
        serverlist = [[self.bot.get_user(int(friend[0])).name, friend[1]] for friend in serverlist]
        em = discordembedpages.embedpages(serverlist, f"Friend ID's for {ctx.guild.name} Server.",
                                          reactfootertopages=True,
                                          inline=False, intochunks=5)

        msg = await ctx.send(embed=em.getembed())
        em.setmsg(msg, ctx.author)
        for x in discordembedpages.reactdefaults():
            await msg.add_reaction(x)
