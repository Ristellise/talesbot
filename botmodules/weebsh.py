from discord.ext import commands
from Utilities import Utils
import Dconst
import discord
import aiohttp,typing


class weebsh:
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command()
    async def weeb(self, ctx, simtype):
        """
        Raw command to access weeb.sh specific image tags
        simtype: type of image
        return: image/gif. else a 404 or error message in an embed
        """
        url = Utils.weeb(False, imgtype=simtype).weeburl()
        async with aiohttp.ClientSession() as clientsession:
            async with clientsession.get(url, headers=Dconst.wolketoken) as resp:
                req = resp
        if req.status in [404, '404']:
            return await ctx.send(content="", embed=discord.Embed().set_image(url='https://http.cat/404.jpg'))
        elif req.status not in ['200', 200]:
            return await ctx.send(f"Error: ```{req.status}```")
        url = await req.json()
        url = url['url']
        hugembed = discord.Embed(title=f"{simtype}")
        hugembed.set_image(url=url)
        hugembed.set_footer(text="Powered by weeb.sh (Thanks to Wolke!)")
        await ctx.send(embed=hugembed)

    @commands.guild_only()
    @commands.command()
    async def hug(self, ctx, user: typing.Optional[discord.Member] = None):
        """
        grabs a hug gif/images from weeb.sh
        user: hug to who?
        returns: hug image/gif in an embed.
        """
        await do_interact(user, ctx, "hugged", "hug", self.bot)

    @commands.guild_only()
    @commands.command()
    async def slap(self, ctx, user: typing.Optional[discord.Member] = None):
        """
        Slap someone for being a baka.
        user: hug to who?
        returns: hugs
        """
        await do_interact(user, ctx, "slapped", "slap", self.bot)

    @commands.guild_only()
    @commands.command()
    async def punch(self, ctx, user: typing.Optional[discord.Member] = None):
        """
        Punch someone for being a complete idiot.
        May or may not include Inami-chan.
        user: hug to who?
        returns: Punches
        """
        await do_interact(user, ctx, "punched", "punch", self.bot)

    @commands.guild_only()
    @commands.command()
    async def pout(self, ctx, ):
        await do_image(ctx, "pout", "Aww look you made them all pouty!")

    @commands.guild_only()
    @commands.command()
    async def dab(self, ctx, user: typing.Optional[discord.Member] = None):
        await do_interact(user, ctx, "dabbed", "dab", self.bot)

    @commands.guild_only()
    @commands.command()
    async def poke(self, ctx, user: typing.Optional[discord.Member] = None):
        await do_interact(user, ctx, "poked", "poke", self.bot)

    @commands.guild_only()
    @commands.command()
    async def owo(self, ctx):
        await do_image(ctx, 'owo', title="OwO Whats this?")

    @commands.command()
    async def insult(self, ctx, user: typing.Optional[discord.Member] = None):
        await do_interact(user, ctx, "insulted", "insult", self.bot)

    @commands.command()
    async def pat(self, ctx, user: typing.Optional[discord.Member] = None):
        await do_interact(user, ctx, "patted", "pat", self.bot)

    @commands.command(name="dmemes")
    async def discord_memes(self, ctx):
        await do_image(ctx, 'discord_memes', title="JUST discord memes...")

    @commands.command(name="greet")
    async def greeter(self, ctx):
        await do_image(ctx, 'greet', title="Ohayou~!")

    @commands.command(name="neko")
    async def nyaa(self, ctx):
        await do_image(ctx, 'neko', title="N-Nyaa~!")

    @commands.command()
    async def awoo(self, ctx):
        await do_image(ctx, 'awoo', title="Awoo!")

    @commands.command()
    async def nani(self, ctx):
        await do_image(ctx, 'nani', title="Omae Wa Mou Shindeiru")


async def do_image(ctx, imgtype, title="No Title! \\o/"):
    url = await Utils.asyncrequest('GET', Utils.weeb(False, imgtype=imgtype).weeburl(),
                                   dojson=True, headers=Dconst.wolketoken)
    url = url['url']
    hugembed = discord.Embed(title=title)
    hugembed.set_image(url=url)
    hugembed.set_footer(text="Powered by weeb.sh (Thanks to Wolke!)")
    await ctx.send(embed=hugembed)


async def do_interact(user, ctx, interact, interact_type, bot, flip=False):
    if user is None:
        selfhug = True
        user = "<@!352822626479243264>"
    else:
        selfhug = False
    usermember = user.display_name
    url = await Utils.asyncrequest('GET', Utils.weeb(False, imgtype=interact_type).weeburl(),
                                   dojson=True, headers=Dconst.wolketoken)
    url = url['url']
    if selfhug is False:
        if flip:
            hugembed = discord.Embed(title=f"{usermember} {interact} {ctx.author.display_name}!")
        else:
            hugembed = discord.Embed(title=f"{ctx.author.display_name} {interact} {usermember}!")
    else:
        hugembed = discord.Embed(title=f"{ctx.author.display_name} {interact} {usermember}!")
    hugembed.set_image(url=url)
    hugembed.set_footer(text="Powered by weeb.sh (Thanks to Wolke!)")
    await ctx.send(embed=hugembed)
