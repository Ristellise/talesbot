#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

import aiohttp
import discord
from discord.ext import commands

import Dconst
from Utilities import Utils
from apis import whatanime as waga
from apis import saucenao as saucy
from apis import ocrspace as ocr
from apis import imagetoosl as imagetools
from apis import safebooru as sb
import textwrap
import numpy
import asyncio


# [title, thumb, similarity, saucesite, sauceurl]
class search:
    def __init__(self, bot):
        self.bot: commands.Bot = bot

    @commands.guild_only()
    @commands.command(name="saucenao", aliases=['saucethis', 'sauce'])
    async def saucenao(self, ctx):
        if len(ctx.message.attachments) >= 1:
            attatchment = ctx.message.attachments[0]
            if attatchment.height is not None:
                msg = await ctx.send("Processing your image. please hold.")
                data = await saucy.saucenao(attatchment.url)
                if data[0] == 2:
                    return await ctx.send(f"Error Occured: ```{data[1]}```")
                else:
                    dembed = discord.Embed(title=f"Sauce Found?:")
                    if data[0] is None:
                        dembed.add_field(name="Title:", value="N/A")
                    else:
                        dembed.add_field(name="Title:", value=data[0])
                    dembed.add_field(name="Similarity", value=f"{data[2]}%")
                    dembed.set_thumbnail(url=data[1])
                    dembed.add_field(name="Source Site:", value=f"{data[3]}")
                    dembed.add_field(name="Source URL: ", value=f"{data[4]}")
                    dembed.add_field(name="Warning:", value="__**[Source URL's May not be SFW. Here be Dragons!]**__")
                    dembed.set_footer(text="Powered by SauceNAO API")
                    if round(float(data[2])) < 50:
                        dembed.add_field(name="Note:",
                                         value="Similarity is less that 50%. It is highly likely that this isnt the "
                                               "correct source.")
                    return await msg.edit(content="", embed=dembed)
            else:
                return await ctx.send("Not a valid Image File!")
        elif ctx.message.content not in ["", None]:
            try:
                url = re.search(r"(?P<url>https?://[^\s]+)", ctx.message.content).group("url")
            except AttributeError:
                return ctx.send("No URL Found!")
            # Do sanity check. if its actually a real image or not.
            if not Utils.validate_url(url):
                return await ctx.send("Invalid URL!")
            msg = await ctx.send("Processing your image. please hold.")
            data = await saucy.saucenao(url)
            if data[0] == 2:
                return await ctx.send(f"Error Occured: ```{data[1]}```")
            else:
                dembed = discord.Embed(title=f"Sauce Found?")
                if data[0] is None:
                    dembed.add_field(name="Title:", value="N/A")
                else:
                    dembed.add_field(name="Title:", value=data[0])
                dembed.set_thumbnail(url=data[1])
                dembed.add_field(name="Similarity", value=f"{data[2]}%")
                dembed.add_field(name="Source Site:", value=f"{data[3]}")
                dembed.add_field(name="Source URL: ", value=f"{data[4]}")
                dembed.add_field(name="Warning:", value="__**[Source URL's May be NSFW. Here be Dragons!]**__")
                dembed.set_footer(text="Powered by SauceNAO API")
                if round(float(data[2])) <= 50:
                    dembed.add_field(name="Note:",
                                     value="Similarity is less or equal to 50%. It is highly likely that this isn't the"
                                           " correct source.")
                return await msg.edit(content="", embed=dembed)
        else:
            return await ctx.send("No attachments or valid links provided!")

    @commands.guild_only()
    @commands.command(name="whatanime", aliases=['waga', 'wa', ])
    async def whatanimega(self, ctx):
        if len(ctx.message.attachments) >= 1:
            attatchment = ctx.message.attachments[0]

            if attatchment.height is not None:
                msg = await ctx.send("Processing your image. please hold.")
                data = await waga.processurl(attatchment.url, loop=self.bot.loop)
                if data[0] == 2:
                    return await ctx.send(f"Error Occured: ```{data[1]}```")
                else:
                    dembed = discord.Embed(title=f"Anime: {data[1]}")
                    dembed.add_field(name="Similarity", value=f"{round(data[2]*100,1)}%")
                    dembed.add_field(name="AniList URL: ", value=f"{data[3]}")
                    dembed.set_footer(text="Powered by whatanime.ga (Thanks to Soruly!)")
                    if round(data[2] * 100, 1) < 80:
                        dembed.add_field(name="Note:",
                                         value="Similarity is less that 80%. It is highly likely that this isnt the "
                                               "anime your looking for.")
                    return await msg.edit(content="", embed=dembed)
            else:
                return await ctx.send("Not a valid Image File!")
        elif ctx.message.content not in ["", None]:
            try:
                url = re.search(r"(?P<url>https?://[^\s]+)", ctx.message.content).group("url")
            except AttributeError:
                return ctx.send("No URL Found!")
            # Do sanity check. if its actually a real image or not.
            if not Utils.validate_url(url):
                return ctx.send("Invalid URL!")
            msg = await ctx.send("Processing your image. please hold.")
            data = await waga.processurl(url)
            if data[0] == 2:
                return await ctx.send(f"Error Occured: ```{data[1]}```")
            else:
                dembed = discord.Embed(title=f"Anime: {data[1]}")
                dembed.add_field(name="Similarity", value=f"{round(data[2]*100,1)}%")
                dembed.add_field(name="AniList URL: ", value=f"{data[3]}")
                dembed.set_footer(text="Powered by whatanime.ga (Thanks to Soruly!)")
                if round(data[2] * 100, 1) < 80:
                    dembed.add_field(name="Note:",
                                     value="Similarity is less that 80%. It is highly likely that this isnt the "
                                           "anime your looking for.")
                return await msg.edit(content="", embed=dembed)
        else:
            return await ctx.send("No attachments or valid links provided!")

    @commands.guild_only()
    @commands.command(name="ocrspace", aliases=['ocrthis', 'ocr'])
    async def ocrthis(self, ctx, url=None, lang='eng'):
        """
        Use OCR on to a specific image
        url may not be sepcified, but can be sent as an attatchment and url becomes the language.
        If lang is none, default is english
        Currently supports: (lang = 3 dletter code specified below.)
        Arabic(ara), Bulgarian(bul), Chinese(Simplified)(chs), Chinese(Traditional)(cht),Croatian(hrv), Czech(cze),
        Danish(dan), Dutch(dut), English(eng), Finnish(fin), French(fre), German(ger), Greek(gre), Hungarian(hun),
        Korean(kor), Italian(ita), Japanese(jpn), Norwegian(nor), Polish(pol), Portuguese(por), Russian(rus),
        Slovenian(slv), Spanish(spa), Swedish(swe), Turkish(tur)
        """
        if len(ctx.message.attachments) >= 1:
            # url is a lang... probably
            if url is not None:
                if len(url) == 3:
                    lang = url
            attatchment = ctx.message.attachments[0]
            if attatchment.height is not None:
                msg = await ctx.send("Processing your image. please hold.")
                data = await ocr.ocrthis(attatchment.url, lang=lang)
                if data[0] == 2:
                    return await ctx.send(f"Error Occured: ```{data[1]}```")
                else:
                    dembed = discord.Embed(title=f"Done!")
                    data[1] = data[1].replace('\r', '')
                    if data[1] != "":
                        dembed.add_field(name="Text:", value=f"{data[1]}")
                    else:
                        dembed.description = "OCR Returned nothing..."
                    return await msg.edit(content="", embed=dembed)
            else:
                return await ctx.send("Not a valid Image File!")
        elif url is not None:
            # Do sanity check. if its actually a real image or not.
            if not Utils.validate_url(url):
                return ctx.send("Invalid URL!")
            msg = await ctx.send("Processing your image. please hold.")
            data = await ocr.ocrthis(url, lang=lang)
            if data[0] == 2:
                return await ctx.send(f"Error Occured: ```{data[1]}```")
            else:
                dembed = discord.Embed(title=f"Done!")
                data[1] = data[1].replace('\r', '')
                if data[1] in ['', None]:
                    data[1] = ":warning: Text Not found."
                dembed.add_field(name="Text:", value=f"{data[1]}")
                return await msg.edit(content="", embed=dembed)
        else:
            return await ctx.send("No attachments or valid links provided!")

    @commands.guild_only()
    @commands.command(name='xkcd')  # Done
    async def xkcd(self, ctx, gid=None):
        """
        Grabs XKCD Comic
        Get's latest one if gid is not specifiec.
        return: XKCD Comic Embed.
        """
        embed = discord.Embed()
        embed.colour = discord.Colour.orange()
        if gid is None:
            embed.description = "Grabbing latest comic\n<a:loading:417299545404473345>"

            comic = await ctx.send(embed=embed)
        else:
            embed.description = f"Grabbing {gid} comic ID\n<a:loading:417299545404473345>"
            comic = await ctx.send(embed=embed)
        try:
            gid = int(gid)
        except ValueError:
            await comic.delete()
            return await ctx.send(":warning: ID is not a string")
        except TypeError:
            # This is fine... that just means no ID is passed
            pass

        if gid:
            resp = await Utils.asyncrequest('GET', f"http://xkcd.com/{gid}/info.0.json")
        else:
            resp = await Utils.asyncrequest('GET', f"http://xkcd.com/info.0.json")
        if resp.status == 404:
            embed.description = f"Can't find Ay Comic matching: {gid}."
            embed.colour = discord.Colour.dark_red()
            await comic.edit(content="", embed=embed)
            return
        elif resp.status is not 200:
            embed.description = f"Encounter an Error: ```{resp.status}```"
            embed.colour = discord.Colour.dark_red()
            await comic.edit(embed=embed)
        else:
            json = await resp.json()
            embed.title = json.get('safe_title')
            embed.set_image(url=json.get('img'))
            embed.set_footer(text=json.get('alt'))
            embed.colour = discord.Colour.green()
            embed.set_author(name="Randall Munroe", url=f"http://xkcd.com/{json.get('num')}")
            embed.description = ""
            embed.url = f"http://www.explainxkcd.com/wiki/index.php/{json.get('num')}"
            await comic.edit(embed=embed)

    @commands.command("safebooru", aliases=['sb', 'sbart', 'sba'])
    async def safebooru(self, ctx, *, searchterm):
        """
            Safebooru Image Search Engine.
            searchterm:

            --- Cheat Sheet ---
            The guide to Safebooru:
            tag1, tag2 [Search for posts that have tag1 and tag2. NOTE: THE COMMA IS IMPORTANT.]
            {abc~} [Search fuzzyily] according to \"Levenshtein distance\".
            `rita*dio` [Search for posts with tags that starts with rita and ends with dio]
            `!tag} [Search for posts that don't have tag]
            --- ----------- ---
            Note: This is all based on Ristelle's judgement. Mods:
            if you find an image to be suspecious, let me know at:
            -- Ristelle#6104 --
        """
        sbobject = sb.safebooru(searchterm, True, self.bot.loop)
        message = await ctx.send("Searching...")
        jsonobject = await sbobject.getrandomimage()
        if jsonobject is not None:
            await message.edit(content="Found Image. Uploading...")
        else:
            await message.delete()
        embed = discord.Embed(title="Try again! Image isn't able to find a suitable safe image.")
        if jsonobject is None:
            return await ctx.send(embed=embed)
        else:
            url = sb.urlformer(jsonobject['image'], jsonobject['directory'], jsonobject['sample'])
            embed.title = "Image Info:"
            embed.description = f"[Direct Link (Maybe a Sample Sized Image)]({url})\n" \
                                f"Searched For: {searchterm}\n" \
                                f"Post link: [Here!](https://safebooru.org/index.php?page=post&s=view&id={jsonobject['id']})"
            tags = ", ".join(jsonobject['tags'].split(" ")).replace("_", " ")
            embed.add_field(name="Tags", value=textwrap.shorten(tags, width=100, placeholder="..."))
            await ctx.send(embed=embed,
                           file=discord.File(sb.imager(url), filename=jsonobject['image']))
            await message.delete()

    @commands.command("sinon")
    async def sinon(self, ctx):
        """
        But why?
        """
        img = ["https://a.doko.moe/cplanf.png",
               "https://a.doko.moe/hccjci.png", "https://a.doko.moe/pjgipa.png"]
        msg = ["...", "Jeez Rist.", "Get some actual help."]
        numpy.random.seed()
        c = numpy.random.choice(img, p=[0.45, 0.1, 0.45])

        if c == img[1]:
            msg = await ctx.send(c)
            for i in msg:
                async with ctx.typing():
                    await asyncio.sleep(1)
                    await ctx.send(i)
            await asyncio.sleep(5)
            await msg.delete()
        else:
            return await ctx.send(c)

    @commands.command("ass")
    async def keropls(self, ctx: commands.Context):
        await ctx.send("Ok")
        return await ctx.send("<:ass:498675574718595112>")