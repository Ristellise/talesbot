import random

import discord
from discord.ext import commands
from apis import sandboxcalc
import copy

import Dconst
from Utilities import Utils
import numpy.random


class MiscellaneousCommands:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='emotelist')
    async def _elist(self, ctx):
        """
        Shows a list of emotes that are provided by this bot.
        """
        elistlocal = []
        for x in Dconst.elist:
            elistlocal.append(f":{x[1]}:\n")
        await ctx.send(f"Available Emotes: ```{''.join(elistlocal)}```")

    @commands.command(name="choose")
    async def choose(self, ctx, *, select: str):
        """
        Choose an item from selection, seperated by a `,`.
        EG: tales!choose Like,This!
        NOTE: if the choices are too similar, the bot will not do the selection.
        """
        clist = [x.strip() for x in select.split(',')]
        for x in clist:
            try:
                if Utils.similar(x, clist[clist.index(x) + 1]) >= 0.95:
                    return await ctx.send(f"<:thinking:449490029547618317> One of these choices are similar...")
            except IndexError:
                pass
        await ctx.send(f"My choice: {random.choice([x.strip() for x in select.split(',')])}")

    @commands.guild_only()
    @commands.command(name="someone")
    async def someone(self, ctx):
        """
        @SOMEONE

        Returns: SOMEONE ELSE
        """
        if ctx.guild is not None:
            elements = [0, 1]
            weight = [0.05, 0.95]
            workingnames = ["Kana Asumi", "Popura Taneshima", "Saki Fujita", "Mahiru Inami", "Eri Kitamura",
                            "Yachiyo Todoroki"]
            easter = numpy.random.choice(elements, p=weight)
            random.seed()
            numpy.random.seed()
            local = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
            if easter == 0:
                local.description = f"[{random.choice(workingnames)}#{random.randint(1000,9999)}]" \
                                    f"(https://www.youtube.com/watch?v=4uou1yGqCG8)"
                return await ctx.send(embed=local)
            else:
                local.description = str(random.choice(ctx.guild.members))
                return await ctx.send(embed=local)

    @commands.guild_only()
    @commands.command(name='birb')
    async def birb(self, ctx):
        """
        Gets a random birb image.
        """
        aj = await Utils.asyncrequest('GET', 'https://www.reddit.com/r/birbs/random.json', True)
        try:
            url = aj[0]['data']['children'][0]['data']['media']['oembed']['url']
        except TypeError:
            url = aj[0]['data']['children'][0]['data']['url']
        await ctx.send(f"Birb: {url}")

    @commands.guild_only()
    @commands.command(name="calc",alias=["calculate"])
    async def calc(self, ctx, *, expression):
        em = discord.Embed(title="Result")
        em.description = str(sandboxcalc.calculate(expression))
        em.colour = discord.colour.Colour.blue()
        em.set_footer(text=expression)
        return await ctx.send(embed=em)