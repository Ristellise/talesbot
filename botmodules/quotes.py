from Utilities import Utils, serverroutines
from discord.ext import commands
import discord
from Utilities import discordembedpages, guildjsonutilites
import textwrap


class quoteshook:
    def __init__(self, bot):
        self.bot = bot

    # Base Quote
    @commands.guild_only()
    @commands.group(name="quote", aliases=['q'])  # Done
    async def quote(self, ctx):
        """
        Base Quote command. by default, gets a random quote
        """
        if ctx.invoked_subcommand is None:
            args = ctx.message.content.replace(f'{ctx.prefix}{ctx.invoked_with} ', '') \
                .replace(f'{ctx.prefix}{ctx.invoked_with}', '')
            if args == '':
                args = None
            result = serverroutines.Quotes.get(args, ctx.message.guild.id)
            try:
                quote = result[2]
            except IndexError:
                quote = "Not found"
            qembed = discord.Embed(title=f"{quote}", color=discord.Color.green())
            qembed.description = result[1]
            await ctx.send(embed=qembed)
            await Utils.trydel(ctx)

    # Add Quote
    @commands.guild_only()
    @quote.command()  # Done
    async def add(self, ctx, tag, *, quote: str):
        """
        Add a local-server quote.
        tag: quote tag. something that its 1 word and unique to this quote.
        quote: quote ID
        returns: a message whether the quote has been added or its too long.
        the tag may have a number added to it if a older tag existed with the same word.
        """
        em = discord.Embed()
        if len(quote) >= 2048:
            em.colour = discord.colour.Colour.dark_red()
            em.description = "You know, Discord Embeds doesn't allow for more than 2048 Characters. Sorry!"
        else:
            guildjson = guildjsonutilites.get_instance(ctx.guild.id)
            result = guildjson.quote_add(tag, quote)
            em.colour = discord.colour.Colour.green()
            em.title = "Added quote successfully"
            em.add_field(name="Tag:", value=textwrap.shorten(result[1][0], 128, placeholder="..."))
            em.add_field(name="Quote:", value=textwrap.shorten(result[1][1], 128, placeholder="..."))

    @commands.guild_only()
    @commands.bot_has_permissions(add_reactions=True)
    @quote.command(pass_context=True)
    async def list(self, ctx):
        """
        Returns the list of quotes in the server.
        Be sure it can add emojis!
        :return:
        """
        # returnlist.append([x, textwrap.shorten(sj.serverjson['quote'].get(x), 100, placeholder="...")])

        result = serverroutines.Quotes.list(ctx.guild.id)
        embedpages = discordembedpages.embedpages(result, "List of Quote On this server:", reactfootertopages=True,
                                                  inline=False)
        return await embedpages.messageinitalize(ctx)

    @commands.guild_only()
    @quote.command()
    async def search(self, ctx, *, searchphrase):
        guildjson = guildjsonutilites.get_instance(ctx.guild.id)
        result = guildjson.quote_search(searchphrase)
        if result[0] != 0:
            return await ctx.send(embed=discord.Embed(title=result[1], colour=discord.Colour.dark_red()))
        else:
            embedpages = discordembedpages.embedpages(result[1], f"Search Results for \"{searchphrase}\"",
                                                      reactfootertopages=True,
                                                      inline=False, intochunks=5)
            return await embedpages.messageinitalize(ctx)

    @commands.guild_only()
    @quote.command(pass_context=True)  # Done
    async def get(self, ctx, *, tags: str = None):
        """
        gets a quote.
        If tags is not Defined, gets a random one. Else, tries to get that quote.
        tags: quote tags that is given when you added the new quotes.
        returns: the quote along with the tag in an embed.
        """
        em = discord.Embed()
        if tags is not None:
            tags = tags.strip()
        if tags is None or tags == "":
            em.colour = discord.colour.Colour.dark_red()
            em.title = "Missing Argument"
        else:
            guildjson = guildjsonutilites.get_instance(ctx.guild.id)
            quote = guildjson.quote_get(tags)
            if quote[0] != 0:
                em.colour = discord.colour.Colour.dark_red()
                em.title = quote[1]
            else:
                em.colour = discord.colour.Colour.green()
                em.title = tags
                print(quote)
                em.description = quote[1]
        return await ctx.send(embed=em)

    @commands.guild_only()
    @quote.command()
    async def remove(self, ctx, tag):
        """
        Removes a quote.
        tag: (Required) quote tag
        """
        result = serverroutines.Quotes.remove(tag, ctx.message.guild.id)
        await ctx.send(result[1])

    @remove.error
    async def remove_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            if error.param.name == 'tag':
                await ctx.send("")
