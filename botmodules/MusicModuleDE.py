#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Music Module:
    Definitive Edition
"""
import discord, os
import logging
from discord.ext import commands
import random
import asyncio
import validators
import Dconst
import copy
from Utilities import Utils
import time
import mutagen
from mutagen import mp3


def formatseconds(seconds: float):
    return time.strftime("%H:%M:%S", time.gmtime(seconds))


def parseuploaddate(datestring: str):
    try:
        return f"{datestring[6:8]}/{datestring[4:6]}/{datestring[0:4]}"
    except IndexError:
        return "No/Un/OABC"


_talesnames = ('Alisha', 'Alvin', 'Anise', 'Arche', 'Asbel',
               'Asch', 'Cheria', 'Chester', 'Claus', 'Chloe',
               'Caius', 'Cress', 'Caius', 'Colette', 'Claire',
               'Dio', 'Dezel', 'Edna', 'Eizen', 'Estellise',
               'Estelle', 'Emil', 'Elle', 'Elize', 'Eugene',
               'Harold', 'Guy', 'Gaius', 'Flynn', 'Farah',
               'Hubert', 'Hisui', 'Ix Nieves', 'Judas', 'Jade',
               'Karol', 'Kanonnot', 'Kana', 'Judith', 'Jude',
               'Estellise', 'Kocis', 'Kratos', 'Kyle', 'Kor',
               'Laphicet', 'Lailah', 'Kohaku', 'Kongwai',
               'Leia', 'Leon', 'Lippy', 'Lloyd', 'Ludger',
               'Luke', 'Magilou', 'Mao', 'Malik', 'Marta',
               'Mell', 'Meredy', 'Mika', 'Meebo', 'Milla',
               'Mint', 'Boing-Chan', 'Muzet', 'Nanaly', 'Norma',
               'Pascal', 'Patty', 'Presea', 'Raine', 'Raven',
               'Reala', 'Regal', 'Reid', 'Repede', 'Richard',
               'Rita', 'Ristelle', 'Rokurou', 'Rollo', 'Rody',
               'Rose', 'Rowen', 'Rutee', 'Sara', 'Allen',
               'Senel', 'Sheena', 'Suzu', 'Sorey', 'Sophie',
               'Stahn', 'Tear', 'Veigue', 'Velvet', 'Yuri',
               'Senel', 'Sheena', 'Zephyr', 'Zelos', 'Zavied'
               )
# DO NOT Interface with this directly! you will cause tons of headaches. use VoiceConnectionManager/VCMRetrieve
activeconnections = {}
# Active files.
activefiles = []
logger = logging.getLogger("MusicModuleRemaster")
logger.info("Music Module Remastered System 2.0 Loading...")


def VCMRetrieve(ctx: commands.Context, check_only=False):
    """
    Searches 'activeconnections' for a guild.
    if it Fails, it generates a new one and returns it.
    Args:
        ctx: discord Context Class

    Returns: VoiceConnectionManager

    """
    for k, v in activeconnections.items():
        if k == str(ctx.guild.id):
            return v
    if not check_only:
        return VoiceConnectionManager(ctx)
    else:
        return False


def generatetales():
    """
    Generates a 2 Word Tales code name.
    Returns: String of a 2 word code name.
    """
    random.seed()
    return f"{random.choice(_talesnames)}|{random.choice(_talesnames)}"


class YTDL:
    @staticmethod
    def search(searchterm):
        results = Dconst.cachedynamicytdl[1].extract_info(searchterm, download=False, )
        return results


class YTDLSrc(discord.PCMVolumeTransformer):
    def __init__(self, source, volume=0.5, *, data):
        super().__init__(source, volume)

    @classmethod
    async def from_url(cls, url, *, loop=None):
        loop = loop or asyncio.get_event_loop()
        ytdl = Dconst.cachedynamicytdl[0]
        data = await loop.run_in_executor(None, ytdl.extract_info, url)
        if data is None:
            data = await loop.run_in_executor(None, ytdl.extract_info, url, False)
        filename = (ytdl.prepare_filename(data))
        # ytdata is a copy of data with filename as a param
        return cls(discord.FFmpegPCMAudio(filename, before_options=Dconst.ffbefopts, options=Dconst.ffopts), data=data)


class VCMError(Exception):
    """
    Generic VCMError.
    """
    pass


class VoiceConnectionManager:
    def __init__(self, Context: commands.Context):
        """
        VoiceConnectionManager. Does all the connection, stop, skips, overlap checking and a ton of other stuff.
        Args:
            Context:
        """
        self.ctx = Context
        self.server = self.ctx.guild
        self.channel = self.ctx.author.voice.channel
        self.client = None
        self.event = asyncio.new_event_loop()
        self.code = generatetales()
        self.queue = asyncio.Queue()
        self.current = None
        self.exists = False
        self.stopskip = [set(), set()]
        self.vcmlogger = logging.getLogger(f"VCMInstance|{self.code}")

    def ss_add(self, ss_type: int, userid):
        if 0 <= ss_type <= 1:
            self.stopskip[ss_type].add(userid)
            self.sync()
            return self.stopskip[ss_type]
        else:
            raise VCMError(f"Invalid SS param! :{ss_type}")

    def ss_clear(self, ss_type: int):
        if 0 <= ss_type <= 1:
            self.stopskip[ss_type].clear()
            self.sync()
            return self.stopskip[ss_type]
        else:
            raise VCMError(f"Invalid SS param! :{ss_type}")

    def ss_get(self, ss_type: int):
        if 0 <= ss_type <= 1:
            return self.stopskip[ss_type]
        else:
            raise VCMError(f"Invalid SS param! :{ss_type}")

    async def queue_add(self, item, info_dict):
        self.vcmlogger.info(f"Adding to queue...")
        await self.queue.put([item, info_dict])
        self.sync()

    def queue_list(self):
        return self.queue._queue

    def queue_get(self):
        try:
            item = self.queue.get_nowait()
            self.sync()
            return item
        except asyncio.QueueEmpty:
            return None

    def queue_empty(self):
        return self.queue.empty()

    def update_current(self, current):
        self.current = current
        self.sync()
        return True

    def sync(self):
        activeconnections[str(self.server.id)] = self

    async def createconnection(self):
        connection = activeconnections.get(str(self.server.id), None)
        if connection is None and self.channel is not None:
            self.client = await self.channel.connect()
            logger.info(f"{self.server.id} Does not Exist. Connecting...")
            self.sync()
            logger.info("Connection OK. Assigned to activeconnections.")
            self.exists = True
            return ['Joined', 0, self.channel, activeconnections[str(self.server.id)]]
        elif self.channel is None:
            logger.critical("Cannot connect to channel because channel is not provided!")
        else:
            logger.info(f"{self.exists} {self.channel.id} {connection.get('channel').id}")
            if self.channel.id != connection['channel'].id:
                await connection['client'].move_to(self.channel)
                return ['Moved', 1, self.channel, activeconnections[str(self.server.id)]]
            elif self.exists:
                return ['Already in', 2, self.channel, activeconnections[str(self.server.id)]]
            else:
                raise VCMError(f"Voice is in an Invalid State! {connection}")

    async def closeconnection(self):
        self.vcmlogger.info("Disconnecting client...")
        if self.client.is_playing():
            self.client.stop()
        await self.client.disconnect()

        self.vcmlogger.info("Removing from database...")
        activeconnections.pop(str(self.server.id))

    @staticmethod
    def shutdown(bot: commands.Bot):
        loop = bot.loop
        for connection in activeconnections:
            logger.info(f"Stopping: {connection}")
            asyncio.ensure_future(connection.get('client').disconnect(), loop=loop)
            logger.info(f"Closed: {connection}")
        VoiceConnectionManager.wipe_inactive()

    @staticmethod
    def wipe_inactive():
        root = os.path.join(os.getcwd(), 'audiofiles')
        for file in os.listdir(root):
            try:
                os.remove(file)
            except PermissionError:
                logger.info(f"Unable to remove  \"{fullpath}\". Because its being accessed.")

    @staticmethod
    def wipe_file(file, simulate=False):

        root = os.path.join(os.getcwd())
        fullpath = os.path.join(root, file)
        if os.path.isfile(os.path.join(root, file)):
            if simulate:
                logger.info(f"Simulating deletion of: \"{fullpath}\"")
            else:
                try:
                    os.remove(fullpath)
                except PermissionError:
                    logger.info(f"Unable to remove  \"{fullpath}\". Because its being accessed.")
        else:
            logger.warning(f"Passed invalid file: \"{fullpath}\" Does not exist!")

    @staticmethod
    def retrieveconnection(guildid):
        return activeconnections.get(str(guildid))


class music:

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    def __unload(self):
        logger.info("Unloading all connections...")
        VoiceConnectionManager.shutdown(self.bot)

    @staticmethod
    async def _disconnect(ctx):
        connection = VCMRetrieve(ctx)
        if not connection.exists:
            return await ctx.send("Bot is not connected to any channel here.")
        msg = await ctx.send(f"Disconnecting from: `#{connection.channel}`")
        await connection.closeconnection()
        await msg.edit(content=f"Disconnected from: `#{connection.channel}`")

    async def process_url(self, url):
        video = Dconst.cachedynamicytdl[0].extract_info(url, download=False)
        if video.get('duration', 0) < 900:
            return {'status': 0, 'data': video}
        return {'status': 1}

    @staticmethod
    async def is_connected(ctx: commands.Context):
        if ctx.author.voice is None:
            await ctx.send("Your not connected to a voice channel!")
            return False
        else:
            return True

    def sync_play_next(self, ctx, connection: VoiceConnectionManager, data=None, after_data=None):
        if after_data is not None:
            file = Dconst.cachedynamicytdl[0].prepare_filename(after_data)
            connection.wipe_file(file)
        coro = self.play_next(ctx, connection, data=data)
        # This will return.
        return asyncio.ensure_future(coro, loop=self.bot.loop)

    async def play_next(self, ctx, connection: VoiceConnectionManager, data):
        if connection.client.is_playing() and data is not None:
            embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
            embed.title = "Added Song to Queue."
            embed.add_field(name="Title", value=data.get('title', 'A Nameless song'), inline=False)
            embed.add_field(name="URL", value=f"[Click Me!]({data.get('webpage_url')})", inline=False)
            embed.add_field(name="Requested by", value=ctx.author.display_name, inline=False)
            return await ctx.send(embed=embed)
        else:
            if connection.queue_empty():
                # Empty queue. stop.
                return {'status': 0, 'reason': "No songs in queue."}
            else:
                if connection.client.is_playing():
                    # We escaped from first check. so we are skipping from actually polling again =_=
                    return {'status': -1, 'reason': 'Already playing audio!'}
                queueitem = connection.queue_get()
                if queueitem is None:
                    return {'status': -3, 'reason': 'connection in invalid state?!'}
                else:
                    YT = queueitem[0]
                    data = queueitem[1]
                    connection.update_current(data)
                    await self.send_playing(data, ctx)
                    connection.client.play(YT, after=lambda e: self.sync_play_next(ctx, connection, after_data=data))
                    return {'status': 1, 'reason': 'Started playing...'}

    @commands.command()
    async def skip(self, ctx):
        connection = VCMRetrieve(ctx)
        count = connection.ss_add(1, ctx.author.id)
        if len(count) >= 1 / len(connection.channel.members):
            # Reset connection.
            connection.ss_clear(1)
            connection.client.stop()
        elif ctx.author.id == 175971332784259072:
            connection.ss_clear(1)
            embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
            embed.title = f"Skipping Song: {connection.current.get('title','A Nameless Song')}"
            embed.description = f"Currently at: {len(count)}/{1/len(connection.channel.members)}"
            await ctx.send()
            connection.client.stop()
        else:
            embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
            embed.title = "Added Your vote!"
            embed.description = f"Currently at: {len(count)}/{1/len(connection.channel.members)}"
            return await ctx.send(embed=embed)

    # noinspection PyMethodMayBeStatic
    async def process_search(self, search, searchtype="ytsearch5:"):
        videos = Dconst.cachedynamicytdl[0].extract_info(f"{searchtype}{search}", download=False)
        for video in videos['entries']:
            if video.get('duration', 0) < 900:
                return {'status': 0, 'data': video}
        return {'status': 1}

    async def send_playing(self, data, ctx, title=None):
        embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
        embed.title = "Now Playing..." if title is None else title
        embed.add_field(name="Title", value=data.get('title', 'A Nameless song'), inline=False)
        embed.add_field(name="URL", value=f"[Click Me!]({data.get('webpage_url')})", inline=False)
        embed.add_field(name="Requested by", value=ctx.author.display_name, inline=False)
        duration = data.get('duration', 0.0)
        if duration > 0.0:
            embed.add_field(name="Duration", value=f"{formatseconds(duration)}")
        thumbnail = data.get('thumbnail', None)
        if thumbnail is not None:
            embed.set_thumbnail(url=thumbnail)
        uploaded = data.get('upload_date', None)
        if uploaded is not None:
            embed.add_field(name="Uploaded On", value=parseuploaddate(uploaded))
        return await ctx.send(embed=embed)

    @commands.command(name="queue")
    async def getqueue(self, ctx):
        connection = VCMRetrieve(ctx, check_only=True)
        if connection is False:
            return await ctx.send("Bot is not connected to any channel here.")
        embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('default'))
        msg = await ctx.send("Enumerating list...")
        queue = connection.queue_list()
        if len(queue) == 0:
            return await msg.edit(content="Nothing Currently in the queue!\n"
                                          "To get current song, do `tales!song`")
        for item in connection.queue_list():
            data = item[1]
            user = data.get('discord_user')
            if user is None:
                user = "???"
            else:
                user = user.display_name
            embed.add_field(name=data.get('title', "A Nameless Song"),
                            value=f"Requested by: **{user}** | Duration: **{formatseconds(data.get('duration',0.0))}**",
                            inline=False)
        return await msg.edit(embed=embed, content=None)

    @commands.command(name='song')
    async def getsong(self, ctx):
        """
        Get's the current song
        """
        connection = VCMRetrieve(ctx, check_only=True)
        if connection is False:
            return await ctx.send("Bot is not connected to any channel here.")
        await self.send_playing(connection.current, ctx)

    async def processor(self, data, ctx, connection):
        if data.get('status') == 0:
            data = data.get('data')
        if data.get("extractor") == "generic":
            # We will TRY and get the Metadata.
            meta = get_audio_meta()
        current = await YTDLSrc.from_url(data.get('webpage_url'), loop=self.bot.loop)
        logger.info(current)
        data['discord_user'] = ctx.author
        await connection.queue_add(current, data)
        logger.info(f"Added: {data.get('webpage_url')} to queue")
        # Doesnt return anything. all printing and calling happens here.
        self.sync_play_next(ctx, connection, data)

    @commands.command()
    async def summon(self, ctx):
        if ctx.author.voice is None:
            return await ctx.send("Your not in a voice channel!")
        vcmstate = VCMRetrieve(ctx)
        state = await vcmstate.createconnection()
        logger.info(state)
        if state[1] == 2:
            return
        # noinspection PyTypeChecker
        return await ctx.send(f"{state[0]}: #{state[2].name}")

    @commands.command(aliases=['disconnect'])
    async def stop(self, ctx):
        await self._disconnect(ctx)

    @commands.command()
    async def play(self, ctx: commands.Context, *, url=None):
        if not await self.is_connected(ctx):
            return
        if url is None:
            embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('warn')).description = "No URL's Provided!"
            return await ctx.send(embed=embed)
        valid = validators.url(url, public=True)
        if not valid:
            embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('fatal'))
            if embed is None:
                raise Dconst.DevError("warn Embed is None! did you remove it?!")
            embed.description = f'This is not a URL!\n' \
                                f'if you want to search, do:\n' \
                                f'`{ctx.invoked_with}search`'
            return await ctx.send(embed=embed)
        connection = VCMRetrieve(ctx)
        if not connection.exists:
            await connection.createconnection()
        result = await self.process_url(url)
        if result.get('status', 1) == 1:
            return await ctx.send("Video is too long.")
        else:
            url = result.get('data')
        await self.processor(url, ctx, connection)

    @commands.command()
    async def search(self, ctx, *, term):
        if not await self.is_connected(ctx):
            return
        valid = validators.url(term, public=True)
        if valid:
            embed = copy.deepcopy(Dconst.discordprecoloredembeds.get('fatal'))
            if embed is None:
                raise Dconst.DevError("warn Embed is None! did you remove it?!")
            embed.description = f'This is not a search Term!\n' \
                                f'If you want to search, do:\n' \
                                f'`{ctx.invoked_with}play`'
            return await ctx.send(embed=embed)
        connection = VCMRetrieve(ctx)
        if not connection.exists:
            await connection.createconnection()
        url = await self.process_search(term)
        await self.processor(url, ctx, connection)

    @commands.command(name="db_conn")
    async def debug_connection(self, ctx):
        # please breakpoint here to debug.
        connection = VCMRetrieve(ctx)
        print(connection)


def parse_results(data):
    returndict = {}
    if isinstance(data, str):
        data = data.split("\n")
        resdict = {}

        for i in data:
            resdict[i.split("=")[0]] = "=".join(i.split("=")[1:])
    else:
        resdict = data

    for k, v in resdict.values():
        if "TIT" in k:
            returndict['title'] = k
        elif "TALB" in k:
            returndict['album'] = k
        elif "TPE" in k:
            returndict['artist'] = k
    return returndict


def get_audio_meta(fp):
    metainital = mutagen.File(fp)
    if isinstance(metainital,mp3.MP3):
        res = parse_results(metainital.pprint())
    else:
        res = parse_results(dict(metainital))
    return dict(res)
