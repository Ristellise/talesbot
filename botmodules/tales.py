from discord.ext import commands
import discord
from Utilities import serverroutines, Utils, discordembedpages
from apis import totrwikia
import Dconst
import numpy.random
import random
import os
from collections import Counter
import concurrent
import asyncio
import datetime


class tales_commands:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(no_pm=True, aliases=['link'])  # Done
    async def links(self, ctx):
        """
        :return: Links to all official Tales Channels
        """
        linkbed = discord.Embed(title="Links (Tales of)", color=0x7289da)
        linkbed.add_field(name="Website", value="http://www.talesofgame.com/en/")
        linkbed.add_field(name="Facebook", value="https://www.facebook.com/tales/")
        linkbed.add_field(name="Twitter", value="https://twitter.com/TalesofU")
        linkbed.add_field(name="Blogs?!", value="http://blog.talesofgame.com/")
        linkbed.add_field(name="Tales Channel + (Website)", value="http://tales-ch.jp")
        linkbed.add_field(name="Tales Channel + (Twitter)", value="https://twitter.com/tales_ch")
        await ctx.send(embed=linkbed)

    @commands.command(no_pm=True)  # Done
    async def mentions(self, ctx):
        """
        :return: Links to all Unoffical Tales (Reddit's Websites, etc)
        """
        linkbed = discord.Embed(title="Honorable Mentions:", color=0x7289da)
        linkbed.add_field(name="Prepare to Die Eggbear!", value="https://www.reddit.com/r/tales/")
        linkbed.add_field(name="Aselia", value="http://aselia.wikia.com")
        linkbed.add_field(name="Abyssal Chronicles", value="http://abyssalchronicles.com")
        linkbed.add_field(name="Tales of Wiki", value="http://talesofwiki.wikia.com/wiki/Tales_of_Wiki")
        linkbed.add_field(name="Tales of Transparent", value="http://tales-of-transparent.tumblr.com/")
        linkbed.add_field(name="Tales of Shitpost", value="http://talesofshitpost.tumblr.com/")
        linkbed.add_field(name="Tales of the Rays", value="Do `tales!therays` instead!")
        await ctx.send(embed=linkbed)

    @commands.command(name="gacha")
    @commands.check(Utils.bpyassablecooldown)
    async def gacha(self, ctx, times=None):
        """
        Tales of the Rays JP Luck checker.
        times: 1 - 10
        singles: Use a slightly
        you may not need to put 'times'. it will default to 1.
        """
        if Dconst.gachalimit:
            embed = discord.Embed(title="gacha command is not available during server maintainence!")
            embed.set_image(url='https://i.imgur.com/EYVxfqi.gif')
            return await ctx.send(
                embed=embed)
        numpy.random.seed(int.from_bytes(os.urandom(4), byteorder="little"))
        jp = [[0.05, 0.15, 0.74, 0.06],
              ["<:mirroorbbk:425915216795271168>", "<:4starorb:425915225766887434>",
               "<:3starorb:425915235090825219>", "<:5starorb:425915244489998336>"]]
        jplastroll = [[0.25, 0.25, 0.50],
                      ["<:mirroorbbk:425915216795271168>",
                       "<:5starorb:425915244489998336>",
                       '<:4starorb:425915225766887434>']]
        # 9 rolls [normal rates] [10th role: 25% MA 75% 4*]
        # 10 rolls
        # 6%
        finaltotal = []
        if times is None:
            times = 1
        if int(times) <= 0:
            return await ctx.send("Nani?! Your asking me to do less then 0?")
        elif int(times) >= 1001:
            times = 1
            await ctx.send("Woah! that's way too much! Limiting to 1 time.")
        elif times is None:
            times = 1
        else:
            times = int(times)
        types = jp[1]
        weights = jp[0]
        lrtype = jplastroll[1]
        lrweights = jplastroll[0]
        if times is not None:
            final = []
            for x in range(times):
                numpy.random.seed(int.from_bytes(os.urandom(4), byteorder="little"))
                final.append(numpy.random.choice(types, p=weights))
        for z in range(int(times)):
            final = []
            for x in range(9):
                numpy.random.seed(int.from_bytes(os.urandom(4), byteorder="little"))
                final.append(numpy.random.choice(types, p=weights))
            numpy.random.seed(int.from_bytes(os.urandom(4), byteorder="little"))
            final.append(numpy.random.choice(lrtype, p=lrweights))
            finaltotal += final
        em = discord.Embed(title=f"Your Multipull luck for Today")
        for _, b in enumerate(Counter(finaltotal).items()):
            em.add_field(name=b[0], value=b[1], inline=False)
        await ctx.send(embed=em)

    @commands.command(name='swiki')
    async def wikiasearch(self, ctx, char, *, arguments: str = ""):
        """
        Searches the Tales of the Ray's Wikia
        Args:
            ctx:
            char:
            *arguments:

        Returns:
            Stats
        """
        args = arguments.lower().split()
        if 'weapon' in args or 'weapons' in args or args == []:
            msg = await ctx.send(embed=Dconst.get_loading())
            chardata: totrwikia.totr = await asyncio.get_event_loop().run_in_executor(None, totrwikia.totr, char, True,
                                                                                      True)
            if chardata.status == 0:
                a = discordembedpages.embedpagesdescriptions(chardata.embedlist,
                                                             title="This will Reset soo if your seeing this, please "
                                                                   "report to Ristellise.")
                await msg.edit(embed=a.getembed())
                a.setmsg(msg, ctx.author)
                for x in discordembedpages.reactdefaults():
                    await msg.add_reaction(x)
            else:
                await msg.edit(embed=discord.Embed(title=f"Cannot find: {char}. Check your spelling.",
                                                   color=discord.colour.Colour.dark_red()))