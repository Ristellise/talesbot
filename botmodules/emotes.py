"""
Emotes module.
"""

from Utilities import Utils
import discord
from discord.ext import commands
import re

from Utilities import serverroutines, discordembedpages, guildjsonutilites


class emotes:
    """
    Emote Class.
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.group(name='emote', no_pm=False, aliases=['e'])  # Done
    async def emote(self, ctx):
        """
        Base Emote Command
        :param ctx:
        :return:
        """
        if ctx.invoked_subcommand is None:
            args = ctx.message.content.replace(f'{ctx.prefix}{ctx.invoked_with} ', '').replace(
                f'{ctx.prefix}{ctx.invoked_with}', '')
            jsonf = guildjsonutilites.get_instance(ctx.guild)
            check = jsonf.emote_get(args)
            em = discord.Embed()
            if check[0] == 0:
                em.colour = discord.colour.Color.green()
                em.title = check[1][0]
                em.title = check[1][0]
                em.set_image(url=check[1][1])
                em.description = check[1][2]
                if string[3] != "":
                    em.set_footer(text=check[1][3])
            else:
                em.title = check[1]
            return await ctx.send(embed=em)

    @commands.guild_only()
    @emote.command(no_pm=False)  # Done
    async def remove(self, ctx, tags):
        jsonf = guildjsonutilites.get_instance(ctx.guild)
        check = jsonf.emote_remove(tags)
        em = discord.Embed()
        em.colour = discord.colour.Color.dark_red()
        em.title = check[1]
        if check[0] == 0:
            em.colour = discord.colour.Color.orange()
        return await ctx.send(embed=em)

    @commands.guild_only()
    @emote.command(no_pm=False)  # Done
    async def get(self, ctx, tags):
        """
        Same as with emote: Put in your emote tags after this command
        :param tags: emote tags seperated by spaces.
        :return:
        """
        jsonf = guildjsonutilites.get_instance(ctx.guild)
        check = jsonf.emote_get(tags)
        em = discord.Embed()
        if check[0] == 0:
            em.colour = discord.colour.Color.green()
            em.title = check[1][0]
            em.title = check[1][0]
            em.set_image(url=check[1][1])
            em.description = check[1][2]
            if string[3] != "":
                em.set_footer(text=check[1][3])
        else:
            em.title = check[1]
        return await ctx.send(embed=em)

    @commands.guild_only()
    @emote.command(no_pm=False)  # Done
    async def add(self, ctx, *, squarespace: str):
        """
        Read this command help by doing `!help emote get`
        !e add [Title] [URL] [Tags] [Footer]
        Title: Title of the Embed
        URL: Direct URL link Eg: (https://i.imgur.com/1gSYm4v.jpg)
        Tags: tags for the image
        Footer: Optional footer text.
        Returns an embed if successfully added emote.
        """
        string = re.split(r'\[(.*?)\]', squarespace)
        for k, i in enumerate(string.lstrip().rstrip()):
            string[k] = i.lstrip().rstrip()
        string = list(filter(None, a))
        em = discord.Embed()
        em.colour = discord.colour.Color.dark_red()
        text = None
        if len(string) < 3:
            exprstr = f"Too little parameters. I got only:"
            for i in string:
                exprstr += f"\"{i}\" "
            em.title = exprstr

        elif len(string) > 4:
            exprstr = f"Too Much parameters. I got only:"
            for i in string:
                exprstr += f"\"{i}\" "
            em.title = exprstr
        else:
            if "http" not in string[1]:
                em.title = f"\"{string[1]}\" is not a valid URL. are you missing any Brackets?"
            else:
                if len(string) == 3:
                    string.append("")
                guildf = guildjsonutilites.get_instance(ctx.guild)
                res = guildf.emote_add(string[0], string[1], string[2], string[3])
                if res[0] != 0:
                    em.title = res[1]
                else:
                    em.colour = discord.colour.Color.green()
                    text = "Added Emote! It will look like this."
                    em.title = string[0]
                    em.set_image(url=string[1])
                    em.description = string[2]
                    if string[3] != "":
                        em.set_footer(text=string[3])
        return await ctx.send(content=text, embed=em)

    @commands.guild_only()
    @commands.bot_has_permissions(add_reactions=True)
    @emote.command(no_pm=False)  # Done
    async def list(self, ctx):
        """
        list the current available emotes in this server.
        Server: can be `global` or left blank.
        (global emotes is the emotes from the global server.)
        Returns an Embed List to chat
        """
        server = str(ctx.message.guild)
        elist = serverroutines.Emotes.list(str(ctx.message.guild.id))
        elistembed = discord.Embed(title='list of emotes added in {}'.format(server),
                                   description='Empty emote list!',
                                   color=discord.Color.green())
        if not elist:
            await ctx.send(embed=elistembed)
            return
        else:
            a = discordembedpages.embedpages(elist, f"Emotes for {server}",
                                             reactfootertopages=True,
                                             inline=False,
                                             intochunks=5)
            msg = await ctx.send(embed=a.getembed())
            a.setmsg(msg, ctx.author)
            for x in discordembedpages.reactdefaults():
                await msg.add_reaction(x)
