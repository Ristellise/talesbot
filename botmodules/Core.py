import sys

import asyncio
import time

from botmodules import reg
from Utilities import Utils
from discord.ext import commands
from Utilities import serverroutines, discordembedpages, guildjsonutilites
import discord
import logging, random, Dconst, secrets, textwrap

clog = logging.getLogger("Core")
up_time = time.time()
import os


class Coreo:
    def __init__(self, coreo_bot):
        self.bot = coreo_bot

    @commands.command(aliases=['pf'])
    async def prefix(self, ctx, *, prefix=None):
        """
        Add an alternate Prefix to the bot.
        prefix: Surround the prefix like this `prefix`
        Gives an OK Hand if the bot has added the prefix.
        """
        if prefix is not None:
            prefix = prefix.replace('`', '')
            if prefix == "":
                prefix = None
        jsonf = guildjsonutilites.get_instance(member.guild)
        res = jsonf.get_config("prefix")
        if prefix is None:
            await ctx.send("Clear Prefixes? [Y]es | [N]o")

            def check(m):
                return m.author == ctx.author and m.channel == channel

            msg = await client.wait_for('message', check=check)
            if msg.content.lower() == "yes" or "y" in msg.content.lower():
                jsonf.set_config({"prefix": None})
                return await ctx.send("Cleared Prefixes")
            else:
                if res is None:
                    res = [prefix]
                else:
                    res.append(prefix)
                jsonf.set_config({"prefix": res})
                return await ctx.send(f"Added Prefix: \"{prefix}\".\n"
                                      f"Current list is: {[i for i in res].join(' ')}")

    @commands.command(no_pm=True)
    async def roleids(self, ctx, *, user=None):
        """
        user: @mention
        return: Returns the roles for the user. and its id. Used mainly in setting emoterole.
        """
        if user is None:
            author = ctx.message.author
        elif '<@!' in user:
            user = user.replace("<@!", "").replace(">", "")
            author = ctx.message.guild.get_member(int(user))
        else:
            author = ctx.message.guild.get_member_named(user)
        rolesids = []
        if author.roles is []:
            rolesids.append("None")
        else:
            for author_role in author.roles:
                if str(author_role) == "@everyone":
                    pass
                else:
                    rolesids.append(f"\nName: {str(author_role)} ID: {str(author_role.id)}")
        uinfoembed = discord.Embed(title="Role Info", color=discord.Color.green())
        uinfoembed.add_field(name="Roles", value=','.join(rolesids), inline=True)
        await ctx.send(embed=uinfoembed)

    @commands.command(name='webhookadd')
    async def webhookadder(self, ctx, channel: discord.TextChannel):
        """
        Adds a webhook. Not for the faint-hearted.
        :param ctx:
        :param channel:
        :return:
        """
        perms = (ctx.message.channel.permissions_for(ctx.message.author))
        if perms.administrator or ctx.author.id == 175971332784259072:
            pass
        else:
            return
        if channelobject is None:
            return await ctx.send(f"Cannot find channel : {channel}")
        bytesio = None
        try:
            webhook = await channelobject.create_webhook(name=self.bot.user.name, avatar=bytesio)
        except discord.Forbidden:
            return await ctx.send(
                "I don't have permissions to add webhooks! Specifcally: \"Manage Webhooks\" Permission!")
        await ctx.author.send(f"Done setting up webhook for #{channel}.\n"
                              f"Complete the setup by copy and pasting this webhook url:\n"
                              f"{webhook.url}")

    @commands.command()  # Done
    async def minfo(self, ctx, *, author: discord.Member):
        """
        Get Member Info of the user
        :return: info on the member.
        """
        estatus = "🤔"
        if author.status == author.status.dnd:
            estatus = "<:dnd:417299614102978560>"
        elif author.status == author.status.online:
            estatus = "<:online:417299651923017738>"
        elif author.status == author.status.idle:
            estatus = "<:away:417299626908057610>"
        elif author.status == author.status.invisible:
            estatus = "<:offline:417299637892939790>"
        elif author.status == author.status.offline:
            estatus = "<:offline:417299637892939790>"
        if bool(author.bot):
            authorbot = "<:check:417299583425708032>"
        else:
            authorbot = "<:cross:417299570309988352>"
        try:
            rolesstring = [i.name for i in author.roles[1:]]
        except IndexError:
            rolesstring = []
        uinfoembed = discord.Embed(title="Member Info", color=discord.Color.green())
        uinfoembed.add_field(name="ID", value=author.id, inline=True)
        uinfoembed.add_field(name="Name", value=author.nick, inline=True)
        uinfoembed.add_field(name="Display Name", value=author.display_name, inline=True)
        uinfoembed.add_field(name="currently playing", value=str(author.activity), inline=True)
        uinfoembed.add_field(name="Joined Discord", value="{} UTC".format(author.created_at), inline=True)
        uinfoembed.add_field(name="Joined Server",
                             value="{} UTC".format(author.joined_at.strftime("%Y-%m-%d %H:%M:%S")), inline=True)
        uinfoembed.add_field(name="Status", value=estatus, inline=True)
        if rolesstring in [[]]:
            pass
        else:
            uinfoembed.add_field(name="Roles", value=textwrap.shorten(', '.join(rolesstring),
                                                                      50, placeholder="..."), inline=True)
        if bool(author.bot):
            uinfoembed.add_field(name="Is Bot?", value=authorbot)
        if author.avatar_url == '':
            aurl = author.default_avatar_url
        else:
            aurl = author.avatar_url
        uinfoembed.set_thumbnail(url=aurl)
        await ctx.send(embed=uinfoembed)

    @commands.command()  # Done
    async def birthday(self, ctx, user=None):
        """
        Will give a Happy Birthday to the user.
        :param ctx:
        :param user:
        :return: Happy Birthday Message.
        """
        if user is None:
            await ctx.send("Really? Do i **REALLY** Have to say happy birthday myself?")
        else:
            random.shuffle(Dconst.BirthdayWishes)
            strchoice = secrets.choice(seq=Dconst.BirthdayWishes)
            await ctx.send(strchoice.format(str(user)))

    @commands.command(name="reboot")
    @Dconst.is_owner_with_response()
    async def reboot(self, ctx):
        print("Sys Executable:", sys.executable)
        # We can't really stop the tasks because It will cause the bot to
        # crash out and not execute it correctly.
        await ctx.send("👌 Rebooting...")
        if Dconst.testb:
            os.execl(sys.executable, "python", "MainBot.py")
        else:

            print("launching directly...")
            os.execl(sys.executable, "python3", "MainBot.py")

    @commands.command(no_pm=False, hidden=True)  # DoNotDoc
    @commands.is_owner()
    async def backdoor(self, ctx, action: str, *, cog=None):
        jsonf = guildjsonutilites.get_instance(ctx.guild)
        if action in ['die']:
            choice = secrets.choice(["**Screams in Japanese**", "https://i.imgur.com/suXkSNH.jpg"])
            await ctx.send(choice)
            await self.bot.logout()
            sys.exit(0)
        elif action in ["elist"]:
            Dconst.elist = []
            for x in self.bot.get_guild(345832994050932767).emojis:
                Dconst.elist.append([str(x), x.name])
            await ctx.send("Refreshed Emote lists. Check if it's correct.")
            elistlocal = []
            for x in Dconst.elist:
                elistlocal.append(x[0])
            await ctx.send(f"{' '.join(elistlocal)}")
        elif action in ['leave']:
            for x in self.bot.guilds:
                if x.id == int(cog):
                    clog.info("id matched")
                    await x.leave()
        # Admin to-do list
        elif action in ['elevate']:
            bp = jsonf.get_config('bypass')
            if bp is None or bp is False:
                jsonf.set_config({"bypass": True})
                msg = await ctx.send("Toggled Bypass: True")
                await asyncio.sleep(5)
                return await msg.delete()
            else:
                jsonf.set_config({"bypass": False})
                msg = await ctx.send("Toggled Bypass: False")
                await asyncio.sleep(5)
                return await msg.delete()
        elif action in ['flushroles']:
            if Dconst.confirmflush is False:
                Dconst.confirmflush = True
                return await ctx.send("**WARNING**: ARE YOU SURE? This\n"
                                      "Removes all the Level from members! Enter it again to do so!")
            else:
                await ctx.send("Flushing all Memeber's Roles. This might take a while!")
                roleids = jsonf.get_config('levels-roles')
                roles = []
                for roleid in roleids:
                    roleobject = discord.utils.get(ctx.guild.roles, id=roleid)
                    if roleobject is None:
                        return await ctx.send(f"Cannot find: {roleid} in this server.")
                    else:
                        roles.append(roleobject)
                for member in ctx.guild.members:
                    await member.edit(roles=list(set(member.roles) - set(roles)))
                Dconst.confirmflush = False
                return await ctx.send(f"Sucessfuly flushed: {len(ctx.guild.members)} Member of level roles.")
        elif action in ['forceplaying']:
            if cog is None:
                activity = Dconst.getactivity()
                await self.bot.change_presence(activity=activity)
            else:
                for i in Dconst.assets:
                    if i.lower() == cog.lower():
                        await self.bot.change_presence(activity=discord.Game(name=i))
                        break
            clog.info("Forced update on playing status...")
        elif action in ['reassignroles']:
            msg = await ctx.send("Reassigning roles...")
            for member in ctx.guild.members:
                if member.bot:
                    pass
                else:
                    await reg.reg.reflushcheck(ctx, member)
            await msg.edit(content=f"Finished Reassigning Roles")
        elif action in ['eval']:
            if Utils.strmatchwildcase(Dconst.privatevariable, cog):
                return await ctx.send('**HOLD UP**\n'
                                      f'The Heck your trying to do {ctx.author.name}?!')
            try:
                await ctx.send(f'```{str(eval(cog))}```')
            except AttributeError as raw_eval_exception:
                await ctx.send("Failed to Evaluate Message (Attribute Error): ```{}```".format(raw_eval_exception))
            except discord.HTTPException as raw_eval_exception:
                await ctx.send("Failed to Evaluate Message (HTTP Error): ```{}```".format(raw_eval_exception))
            except discord.ClientException as raw_eval_exception:
                await ctx.send("Failed to Evaluate Message (Client Error): ```{}```".format(raw_eval_exception))
            except discord.DiscordException as raw_eval_exception:
                await ctx.send("Failed to Evaluate Message (Discord.py Error): ```{}```".format(raw_eval_exception))
            except BaseException as raw_eval_exception:
                await ctx.send("Failed to Evaluate Message (Unknown Error): ```{}```".format(raw_eval_exception))
        elif action in ['raweval']:
            if Utils.strmatchwildcase(Dconst.privatevariable, cog):
                return await ctx.send('**HOLD UP**\n'
                                      f'The Heck your trying to do {ctx.author.name}?!')
            # noinspection PyBroadException
            try:
                await ctx.send(f'{str(eval(cog))}')
            except AttributeError as raw_eval_exception:
                await ctx.send(f"Failed to Evaluate Message (Attribute Error): ```{raw_eval_exception}```")
            except discord.HTTPException as raw_eval_exception:
                await ctx.send(f"Failed to Evaluate Message (HTTP Error): ```{raw_eval_exception}```")
            except discord.ClientException as raw_eval_exception:
                await ctx.send(f"Failed to Evaluate Message (Client Error): ```{raw_eval_exception}```")
            except discord.DiscordException as raw_eval_exception:
                await ctx.send(f"Failed to Evaluate Message (Discord.py Error): ```{raw_eval_exception}```")
            except BaseException as raw_eval_exception:
                await ctx.send(f"Failed to Evaluate Message (Unknown Error): ```{raw_eval_exception}```")

    @commands.guild_only()
    @commands.command(no_pm=True)  # Done
    async def sinfo(self, ctx):
        """
        Gets the Server Info
        :return: Server Info
        """
        cm = ctx.message
        if cm.guild.mfa_level == 1:
            fauth = "Yes"
        else:
            fauth = "No"
        textchannels = len(cm.guild.text_channels)
        voicechannels = len(cm.guild.voice_channels)
        features = '\n'.join(cm.guild.features)
        if features == '':
            features = "None"
        sinfoem = discord.Embed(title="Server info", color=discord.Color.green())
        sinfoem.add_field(name="Server ID", value="{}".format(cm.guild.id))
        sinfoem.add_field(name="Owner", value="{}".format(cm.guild.owner.name))
        sinfoem.add_field(name="Total Members", value="{}".format(str(cm.guild.member_count)))
        sinfoem.add_field(name="Server wide 2FA?", value="{}".format(fauth))
        sinfoem.add_field(name="Voice Server Region", value="{}".format(str(cm.guild.region)))
        sinfoem.add_field(name="(Voice, Text) Channels", value="{}, {}".format(voicechannels, textchannels))
        sinfoem.add_field(name="Special Features", value="{}".format(features))
        sinfoem.add_field(name="Created On", value="{} UTC".format(cm.guild.created_at))
        sinfoem.set_thumbnail(url=cm.guild.icon_url)
        await ctx.send(embed=sinfoem)

    @commands.guild_only()
    @commands.command(no_pm=True, hidden=True)  # Done
    async def nickfix(self, ctx, *, mynick=None):
        perms = (ctx.message.channel.permissions_for(ctx.message.author))
        if ctx.message.author.id == 175971332784259072:
            pass
        elif perms.administrator:
            pass
        else:
            return
        if mynick is None:
            mynick = "Sorey"
        try:
            await ctx.guild.me.edit(nick=mynick)
        except discord.Forbidden:
            await ctx.send("Aww.. i dont have permissions to change my username :frowning:")

    @commands.guild_only()
    @commands.command(no_pm=True)  # Done
    async def purge(self, ctx, limit=100):
        """
        Basically a Purge Chat Command
        :param ctx:
        :param limit: Default(100) You can set it higher. but it might take a longer time.
        :return:
        """
        perms = (ctx.message.channel.permissions_for(ctx.message.author))
        if perms.administrator:
            try:
                await ctx.message.channel.purge(limit=limit)
                await ctx.send("Purged {} Messages.")
            except discord.Forbidden:
                await ctx.send("Forbidden to purge any message. did you give me the right permissions?")
        else:
            await ctx.send('Not an Administrator!')

    @commands.guild_only()
    @commands.command(no_pm=True)  # Done
    async def ping(self, ctx):
        """
        Gets all the shard's latency.
        """
        latencyembed = discord.Embed(title="Latencies for all shards.", description="", color=discord.Color.orange())
        for x in self.bot.latencies:
            shardid, latency = x
            latencyembed.description += f"\nShard: {shardid}| {latency * 1000:.2f} Milliseconds"
        return await ctx.send(content="Pong!", embed=latencyembed)

    @commands.command()
    async def changelog(self, ctx):
        with open('changelog.txt', encoding='utf-8') as f:
            lines = f.read()
        items = lines.split("~~--")
        items = [["Version: ", "\n".join(item.split("\n")[1:])] for item in items]
        a = discordembedpages.embedpages(items, f"Changelog For {Dconst.version[0]}",
                                         reactfootertopages=True,
                                         inline=False,
                                         intochunks=1)
        msg = await ctx.send(embed=a.getembed())
        a.setmsg(msg, ctx.author)
        for x in discordembedpages.reactdefaults():
            await msg.add_reaction(x)


class Moderation:
    def __init__(self, bot):
        self.bot = bot

    # noinspection PyIncorrectDocstring
    @commands.guild_only()
    @commands.command(no_pm=False, aliases=['tm'])  # done
    async def trash_messages(self, ctx, limit=200):
        """
        :param limit: limit number of messages to search for before stopping
        :return: a message saying the number of messages removed.
        """
        messages_is_mine = 0
        async for message in ctx.channel.history(limit=limit):
            if message.author == ctx.guild.me:
                messages_is_mine += 1
                await message.delete()
        await ctx.send(f"Trashed {messages_is_mine} Messages from myself. :put_litter_in_its_place:")

    @commands.command(no_pm=False)  # Done
    async def botinfo(self, ctx):
        """
        :return: Get the info On the bot.
        """
        global up_time
        totalmembers = 0
        for x in self.bot.guilds:
            totalmembers += len(x.members)
        buttime = time.time() - up_time
        author = await self.bot.application_info()
        author = author.name
        m, s = divmod(buttime, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        [cpuusage, rss] = Utils.serverinfo()
        infoembed = discord.Embed(title=ctx.guild.me.display_name, color=discord.Color.green())
        infoembed.add_field(name="Uptime", value=f'{d:.0f} Days {h:.0f}H {m:.0f}M {s:.1f}S')
        infoembed.add_field(name='Bot Version', value='Version: {}'.format('\n'.join(Dconst.version)))
        infoembed.add_field(name='Author', value=f'{author}')
        infoembed.add_field(name='Library:', value="discord.py 1.0.0a0")
        infoembed.add_field(name='Shards Used', value=f'Shards: {self.bot.shard_count}')
        infoembed.add_field(name='Process Info', value=f"CPU usage: {cpuusage} || Memory Used: {rss}")
        infoembed.set_thumbnail(url=self.bot.user.avatar_url)
        await ctx.send(embed=infoembed)

    @commands.command(name="donate")
    async def donate(self, ctx):
        embed = discord.Embed(title=ctx.guild.me.display_name, color=discord.Color.blurple())
        embed.title = "Donate to me senpai~!"
        embed.description = "**[Donation link](https://ko-fi.com/ristellise)**"
        embed.add_field(name="Why?", value="Although I would love to continue working on my bot, "
                                           "I still do have other commitments... "
                                           "Donating will help fund me for future adventures!")
        return await ctx.send(embed=embed)
