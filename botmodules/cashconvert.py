from datetime import datetime

from currency_converter import CurrencyConverter, currency_converter
from discord.ext import commands

from Utilities import Utils


# noinspection PyProtectedMember,PyProtectedMember
class Currency:
    def __init__(self, bot):
        self.bot = bot
        self.c = CurrencyConverter()

    @commands.group(name="cur")
    async def currency(self, ctx):
        """
            Base Command of Currency
            sub commands: `convert` & `rate`
        :param ctx:
        :return:
        """
        if ctx.invoked_subcommand is None:
            await ctx.send("""Available subcommands: `convert (SGD) (USD) (Value in SGD)`/`rate (SGD)`""")

    @currency.command(pass_context=True, aliases=['cc'])
    async def convert(self, ctx, currency, currency2, value):
        try:
            result = self.c.convert(value, currency.upper(), currency2.upper())
            await ctx.send(f"{result:.2f} {currency2} Dollars")
        except ValueError as e:
            await ctx.send(f"Value Error: {e}")

    @currency.command(pass_context=True, aliases=['rt'])
    async def rate(self, ctx, currency):
        """
        get Rate of the :param currency:
        :param ctx:
        :param currency: 3 Digit Code. eg: "JPY,SGD,EUR" etc.
        :return: current rate.
        """
        try:
            rate = self.c._get_rate(currency, date=datetime.now().date())
            await ctx.send(f"Rate: {rate}")
        except currency_converter.RateNotFoundError:
            self.c = CurrencyConverter(fallback_on_wrong_date=True, fallback_on_missing_rate=True)
            rate = self.c._get_rate(currency, date=datetime.now().date())
            await ctx.send(f"Rate: {rate}\nOld Rate(May be in accurate)")

    @currency.command(pass_context=True, aliases=['credit'])
    async def credits(self, ctx):
        """
        :param ctx:
        :return: Show's Credits for the Module
        """
        await ctx.send("Module Using:\n"
                       "https://pypi.python.org/pypi/CurrencyConverter/")

    @currency.command(pass_context=True, aliases=['upd'], hidden=True)
    async def update(self, ctx):
        """
        Updates the Currency Rates.
        :param ctx:
        :return:
        """
        self.c = CurrencyConverter('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')
        await ctx.send("Updated Tables.")
