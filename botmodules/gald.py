"""
    Gald module
"""
from Utilities import rethinkdbaccessorv2
from discord.ext import commands
import discord
import random
import time
from Utilities import humanise
import Dconst
import datetime, logging

logger = logging.getLogger("Gald")
approx = 1000
import dateutil


class gald:

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="gald")
    async def getgald(self, ctx):
        dbinstance = rethinkdbaccessorv2.get_instance(ctx.guild.id)
        gald = dbinstance.get(ctx.author.id).get("gald", 0)
        em = discord.Embed(title=f"{ctx.author.display_name} Gald Balance")
        em.description = f"**{str(gald)}**"
        return await ctx.send(embed=em)

    @commands.command(name="daily", alias=['timely'])
    async def dailygald(self, ctx):
        dbinstance = rethinkdbaccessorv2.get_instance(ctx.guild.id)
        futurets = dbinstance.get(ctx.author.id).get("timestamp_gald", None)
        em = discord.Embed()
        if futurets is None or futurets <= ctx.message.created_at:
            random.seed()
            daily = random.randint(approx - 100, approx + 100)
            dt = datetime.timedelta(days=1)
            ts = ctx.message.created_at + dt
            #  rethinkdbaccessor.update_galdts(ctx.author.id, ts)
            dbinstance.gald_give(ctx.author.id, daily)
            c = random.choice(Dconst.getgald)
            em.title = f"{c[1]}:"
            em.description = c[0].format(name=ctx.author.display_name,
                                         time=str(dt).split(".")[0].split(",")[0],
                                         daily=daily)

        elif futurets > ctx.message.created_at:
            em.description = f"You can't claim this yet! you can claim it in: " \
                             f"{str(futurets - ctx.message.created_at).split('.')[0]}"
            em.colour = discord.colour.Colour.dark_red()
        else:
            logger.warning("Uncaught Else if Statement in dailygald!")
        return await ctx.send(embed=em)

    @commands.command(name="spinner")
    async def galdfidgetspinner(self, ctx, wager=None):
        """
        Fidget Spinner
        wager: The amount of gald you would like to wager on.
        """
        em = discord.Embed()
        try:
            wager = int(wager)
        except ValueError:
            em.description = f"{wager} is not a valid integer."
            em.colour = discord.colour.Colour.dark_red()
            return await ctx.send(embed=em)
        dbinstance = rethinkdbaccessorv2.get_instance(ctx.guild.id)
        take = dbinstance.gald_take(wager)
        if take[1] != 0:
            em.description = f"You don't have Enough Gald to Spinn the Wheel.\n" \
                             f"Balance: **{balance}**"
            em.colour = discord.colour.Colour.red()
            return await ctx.send(embed=em)
        result = gald_backend.do_spinner(wager)
        dbinstance.gald_give(ctx.author.id, result[0])
        em.title = "Wheel Results:"
        em.description = result[1]
        if wager > result[0]:
            em.set_footer(text=f"you got: {result[0]} gald. Too bad!")
        else:
            em.set_footer(text=f"you got: {result[0]} gald. Congrats!")
        return await ctx.send(embed=em)

    @commands.command(name="pull")
    async def pull(self, ctx, pull_set=None):
        if pull_set is None:
            return await ctx.send("You did not specify a set you want to pull!")
class gald_backend:

    @staticmethod
    def do_spinner(gald_paid):
        """
        Does the spinner magic
        Args:
            gald_paid: Gald that is used
        Returns: gald given back and arrow.
        """
        multiplier = [0.5, 1.0, 0.2, 1.3, 2.0, 1.5, 1.7, 2.2]
        arrow = ['↖', '⬆', '↗', '⬅', '➡', '↙', '⬇', '↘']
        random.seed()
        multiplyindex = random.randint(0, len(multiplier) - 1)
        res = int(round(multiplier[multiplyindex] * gald_paid, 0)), arrow[multiplyindex], multiplier

        return res[0], gald_backend.construct_spinner(res)

    @staticmethod
    def construct_spinner(results):
        multipliers = results[2]
        arrow = results[1]
        stringres = f" **『{multipliers[0]}』\t『{multipliers[1]}』\t『{multipliers[2]}]』**\n\n" \
                    f" **『{multipliers[3]}』 ▕{arrow}▏  『{multipliers[4]}』**\n\n" \
                    f" **『{multipliers[5]}』\t『{multipliers[6]}』\t『{multipliers[7]}]』**\n\n"
        return stringres
