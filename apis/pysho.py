"""
pysho
 a Jisho api
"""
import requests
import json

class jisho_sync:

    def __init__(self, word):
        self._word = word
        self.req = None
        self.results = []

    def do(self):
        # Jisho doesn't really allow us to slice words... so we have to do multiple requests.
        while self._word:
            lk = lookup(self._word)
            first = lk.get(0)
            print(self._word)
            self.results.append([first['words'][0], ";".join(lk.get(0)['senses'][0]['english'])])
            if first['words'][0] is None:
                self._word = self._word[1:]
            else:
                self._word = self._word[len(first['words'][0]):]
            if self._word == '':
                break


def lookup(query):
    data = json.loads(requests.get(
        "http://jisho.org/api/v1/search/words?keyword=%s" % query).text)
    results = {}
    for result in range(len(data["data"])):

        results[result] = {"readings": [], "words": [], "senses": {}}

        for a in range(len(data["data"][result]["japanese"])):

            if (data["data"][result]["japanese"][a].get("reading") not
                    in results[result]["readings"]):
                results[result]["readings"].append(
                    data["data"][result]["japanese"][a].get("reading"))

            if (data["data"][result]["japanese"][a].get("word") not
                    in results[result]["words"]):
                results[result]["words"].append(
                    data["data"][result]["japanese"][a].get("word"))

        for b in range(len(data["data"][result]["senses"])):
            results[result]["senses"][b] = \
                {"english": [], "parts": []}

            for c in range(len(data["data"][result]["senses"][b]["english_definitions"])):
                results[result]["senses"][b]["english"].append(
                    data["data"][result]["senses"][b]["english_definitions"][c])

            for d in range(len(data["data"][result]["senses"][b]["parts_of_speech"])):
                results[result]["senses"][b]["parts"].append(
                    data["data"][result]["senses"][b]["parts_of_speech"][d])

    return results
