import asyncio
import io
import os
import random

import requests
from PIL import Image, ImageChops

from Utilities import Utils


async def processimage(url=None, byteio=None, loop=None, resizesize=950):
    if url is None:
        urlbio = io.BytesIO(byteio)
    else:
        resp = await Utils.asyncrequest('GET', url)
        if (len(await resp.read()) / 1024) / 1024 >= 100.0:
            # Something is PROBABLY wrong. do not load into memory!
            return 'NO. LARGE. FILES. EVERRRR!!!!!!'
        else:
            bytesimage = await resp.read()
            urlbio = io.BytesIO(bytesimage)
    if loop is None:
        loop = asyncio.get_event_loop()
    data = await loop.run_in_executor(None, doresize, urlbio, resizesize)
    return data


def syncprocessimage(url=None, byteio=None, resizesize=950):
    if url is None:
        urlbio = io.BytesIO(byteio)
    else:
        requester = requests.get(url)
        urlbio = io.BytesIO(requester.content)
    data = doresize(urlbio, resizesize)
    return data


def doresize(reio: io.BytesIO, resizesize=None):
    urlbio = reio
    if len(urlbio.getvalue()) // 1024 > resizesize:
        resize = True
    else:
        resize = False
    img = Image.open(urlbio)
    if resize:
        while True:
            img = img.resize([img.width // 2, img.height // 2])
            outbio = io.BytesIO()
            img.save(outbio, format="png", optimise=True)
            outbio.seek(0)
            if int((len(outbio.getvalue()) // 1024) * 1.37) > resizesize:
                print(f"Resizing: {len(outbio.getvalue()) // 1024}")
                urlbio = outbio
            else:
                print(f"Done Resizing Final Size: {len(outbio.getvalue()) // 1024}")
                return outbio
    else:
        return urlbio


async def shakeify(image, times):
    if Utils.validate_url(image):
        # In MegaBytes
        resp = await Utils.asyncrequest('GET', image)
        if (len(await resp.read()) / 1024) / 1024 >= 100.0:
            # Something is PROBABLY wrong. do not load into memory!
            return 'NO. LARGE. FILES. EVERRRR!!!!!!'
        else:
            bytesimage = await resp.read()
            image = io.BytesIO(bytesimage)
    im = Image.open(image)
    im = im.convert("RGBA")
    imagelist = []
    # oh shut up pycharm!
    x = None
    xim = None
    for x in range(times):
        w, h = im.size
        random.seed(os.urandom(10))
        xrand = random.randrange(-(w // 20), (w // 20))
        yrand = random.randrange(-(h // 20), (h // 20))
        xim = ImageChops.offset(im, xrand, yrand)
        xim.convert('RGBA')
        if xrand < 0:
            xim.paste((255, 255, 255, 0), (w - abs(xrand), 0, w, h))
        else:
            xim.paste((255, 255, 255, 0), (0, 0, xrand, h))
        if yrand < 0:
            xim.paste((255, 255, 255, 0), (0, h - abs(yrand), w, h))
        else:
            xim.paste((255, 255, 255, 0), (0, 0, w, yrand))
        imagelist.append(xim)
    del x
    del xim

    newim = Image.new('RGBA', im.size)
    newim.paste(imagelist[0])
    returnio = io.BytesIO()
    newim.save(returnio, format='gif', save_all=True, append_images=imagelist[1:], loop=0,
               duration=0.05, disposal=3, optimize=True)
    returnio.seek(0)
    del imagelist
    return returnio
