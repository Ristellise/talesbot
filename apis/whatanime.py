#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64

import Dconst
from Utilities import Utils
from . import imagetoosl


async def processurl(url, loop=None):
    bb64 = await imagetoosl.processimage(url, loop=loop)
    bb64 = base64.b64encode(bb64.getvalue()).decode()
    #
    #
    resp = await Utils.asyncrequest('POST', f"https://trace.moe/api/search",
                                    False, data={'image': bb64})
    if resp.status == 429:
        return [2, "Too many Requests! Please Wait."]
    elif resp.status == 413:
        return [2, f"Image/Gif Too large: {len(bb64)//1024}KB\nThis is our Fault. so here:"
                   f"https://i.imgur.com/QLoJjtB.png"]
    elif resp.status is not 200:
        return [2, f"{resp.text}"]
    reqjson = await resp.json()
    title = reqjson['docs'][0]['title_romaji']
    similarity = reqjson['docs'][0]['similarity']
    anidburl = f"https://anilist.co/anime/{reqjson['docs'][0]['anilist_id']}"
    return [0, title, similarity, anidburl]
