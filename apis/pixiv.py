import json
import discord
import random
class Pix:

    def __init__(self,file):
        with open(file) as f:
            self.data = json.loads(f.read)[:-1]

    def random_pixiv(self):
        random.choice(self.data)

    @staticmethod
    def embed(choice):
        em = discord.Embed()
        em.title = choice.get("title")
        em.description = choice.get("caption")
        em.set_author(name=choice.get("user").get("name"),icon_url=choice.get("user").get("author_logo"),
                      url=choice.get("user").get("url"))
        return em