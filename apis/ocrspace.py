import Dconst
from Utilities import Utils


async def ocrthis(url, lang='eng'):
    json = await Utils.asyncrequest('POST', "https://api.ocr.space/parse/image", dojson=True,
                                    data={'url': url, 'apikey': Dconst.ocrtoken, 'language': lang})
    # capture error
    if json['OCRExitCode'] in [3, 4]:
        return [2, f"OCR Failed to Parse Image: {json['ErrorMessage'][0]}"]
    else:
        results = []
        for x in json['ParsedResults']:
            results.append(x['ParsedText'])
        return [0, '\n'.join(results)]
