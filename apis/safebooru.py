"""
    Safebooru
    The new site to replace zerochan. hopefully without mods to come smashing down.
"""
from Utilities import Utils
import asyncio
import random
import requests
import urllib.request
import json


def imager(url):
    return urllib.request.urlopen(url)


def urlformer(image, directory, issample):
    return f"https://safebooru.org//{'samples' if issample else 'images'}/" \
           f"{directory}/{'sample_' if issample else ''}{image}"


def tagsfixer(tagstring):
    tagslist = tagstring.split(',')
    tagslist = [tag.replace(" ", "_").lstrip("_").lower() for tag in tagslist]
    return tagslist


def tagscorrector(tagslist):
    return " ".join([tag.replace("!", "-") for tag in tagslist])


def tagswrapper(tags):
    return tagscorrector(tagsfixer(tags))


# rating,cover_page,cover,doujin_cover
class safebooru:

    def __init__(self, tags, securesafe=True, loop=None, lewdallow=False):
        # Will be used so we ignore.
        # noinspection PyUnusedLocal
        __doc__ = """
            Safebooru Async search with custom filtering
        :param tags:
        :param securesafe: Filters more uhhh... suspecious images.
        :param loop:
        :param lewdallow:
        Note: This is all based on Ristelle's judgement. Mods:
            if you find an image to be suspecious, let me know at:
            -- Ristelle#6104 --
        """
        r18 = []  # probably a R-18 Rating warning. Better not select it either
        self.tags = tagswrapper(tags)
        # Does even more filtering to some.. questionable images.
        if securesafe:
            self.blacklisttags = ["onsen",
                                  "swimsuits",
                                  "wet clothes",
                                  "o-ring_bikini",
                                  "panties",
                                  'bikini',
                                  "striped",
                                  "bra",
                                  "bust",
                                  "naked towel",
                                  "panties",
                                  "ass",
                                  "pov_feet",
                                  'chikaya',  # not SFW
                                  'folks_(nabokof)',  # not SFW
                                  'kinta_(distortion)',  # not SFW
                                  'genderswap',  # GenderSwap is a hot topic I would not like to touch.
                                  'doujin_cover',
                                  'rating',  # Stop. Get some help.
                                  'leotard']
        else:
            self.blacklisttags = []
        if lewdallow:
            self.blacklisttags.append('breast_envy')
        self.blacklisttags.append(r18)
        self.json = None
        self.loop = loop

    async def callapi(self, pid):
        return await Utils.asyncrequest('GET',
                                        f"https://safebooru.org/index.php?page=dapi&s=post&q=index&tags="
                                        f"{self.tags}&pid={pid}&json=1",
                                        dojson=True)

    def callapisync(self, pid):
        return requests.get(f"https://safebooru.org/index.php?page=dapi&s=post&q=index&tags="
                            f"{self.tags}&pid={pid}&json=1").json()

    async def callandcheck(self):
        while True:
            if userequests:
                self.json = self.callapisync(random.randint(0, 5))
            else:
                self.json = await self.callapi(random.randint(0, 5))
            if self.json:
                print("this below is a json")
                print(self.json)
                print("--------------")
                return
            elif self.json is None:
                print(self.json)
                return


    async def getrandomimage(self):
        if self.json is None:
            await self.callandcheck()
        attempts = 0
        requestattempts = 0
        while True:
            try:
                choice = random.choice(self.json)
            except TypeError:
                return None
            for x in choice['tags'].split(" "):
                if x in 'official_art':
                    #  Well it is considered official art soo I guess it's safe then...
                    return choice
                elif x in self.blacklisttags or choice['rating'] != 'safe':
                    pass
                else:
                    return choice
                attempts += 1
                if attempts < 10 and requestattempts < 2:
                    #  mark this set as cursed. draw a new card.
                    await self.callandcheck()
                    requestattempts += 1
                    attempts = 0
                elif requestattempts == 2:
                    # Fk it
                    return None


userequests = False
