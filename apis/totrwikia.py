from bs4 import BeautifulSoup
import wikia, os
from Utilities import Utils
import discord
import urllib.parse
import operator
import heapq
import textwrap

wikia.set_user_agent("YuriBotWikia/1.0.0 (https://gitlab.com/Ristellise/talesbot)")
subwiki = 'tales-of-the-rays'

"""
Tales of the Rays Wikia Character Data Searcher
TotRWCDS
"""

formatting = """```md
<Name: {}>
<Arte: {}>
<Type: {}>
<Element: {}>
<Rarity: {}>
+~[LVL]~|~[PHY]~|~[ART]~|~[CC]~+
|  {}|  {}|  {}|  {}|
|  {}|  {}|  {}|  {}|
|  {}|  {}|  {}|  {}|
|  {}|  {}|  {}|  {}|
|  {}|  {}|  {}|  {}|
|  {}|  {}|  {}|  {}|
+~~~~~~~~~ Enhancements ~~~~~~~+
|  {}
|  {}
|  {}
|  {}
|  {}
+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```"""

charformatstring = """```css
+~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~+
|  <Full Name>  | {name-full[0]}
|               | {name-full[1]}
|  <Japanese>   | {name-jp[0]}
|   Name>       | {name-jp[1]}
|  <Gender>     | {gender}
|               | {genderplus}
|  <Anima Sync> | [{anima}]
|  <Game>       | {game[0]}
|               | {game[1]}
+~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~+
|  <Resists>    | [{resistance}]
|  <JP VA>      | [{jp-voice}]
|  <Age>        | [{age}]
|  <Height>     | [{height}]
|  <Weight>     | [{weight}]
|  <Weapon>     | [{weapon}]
+~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~+
```"""


class totr:
    def __init__(self, page=None, donumerate=False, instantembed=False):
        if page:
            try:
                self.pager = wikia.page(subwiki, page)
                self.status = 0
            except wikia.WikiaError as e:
                if 'find page' in str(e):
                    self.pager = None
                    self.status = 404
                    self.msg = "Not found"
                else:
                    self.pager = None
                    self.status = 200
                    self.msg = e
        else:
            self.pager = wikia.search(subwiki, page)
        if self.status == 0:
            self.chardata = None
            self.parse_characterhtml()
            self.charweap = None
            self.embedlist = None
            if donumerate:
                self.enumerate_weapons()
            if instantembed:
                self.embedlist = self.construct_weapons()

    # noinspection PyMethodMayBeStatic
    def parse_weaponhtml(self, html, name, artename):
        soup = BeautifulSoup(html, "lxml")
        alldata = soup.find_all('table', {'class': 'wikitable'})
        lbstats = []
        table_data = [[cell.text for cell in row("td")] for row in alldata[0]("tr")]
        for idx, a in enumerate(table_data):
            if len(table_data[idx]) != 0:
                lbstats.append(table_data[idx])
        obtain = [[cell.text for cell in row("td")] for row in alldata[1]("tr")][1][0]
        arte = [[cell.text for cell in row("td")] for row in alldata[2]("tr")][2][1:3]
        raw_skills = [a.text for a in alldata[3].find_all('th')]
        skills = []
        for a in raw_skills[4:-1]:
            if a == "Items needed":
                pass
            else:
                skills.append(a)
        asidedata = []
        sidetab = soup.find("aside")
        for i in sidetab:
            if "type" in str(i):
                url = i('a')[0].get('href')
                spath = urllib.parse.urlsplit(url).path
                if '-' in spath:
                    for a in spath.split('/'):
                        if '-_' in a:
                            asidedata.append(a.split('.')[0].split('_'))
                            break
        return {'name': [name, artename], 'lb': lbstats, 'obtained': obtain, 'arte': arte, 'skills': skills,
                'sidedata': asidedata, "image": sidetab.find_all("a")[0]["href"]}

    def enumerate_weapons(self):
        """
        Loops through all the weapon stats and get's their respective stats
        Returns: 'dict' for all the weapons.
        """
        if self.chardata is None:
            self.parse_characterhtml()
        weapons = self.chardata.get('weps')
        rweps = []
        for weapon in weapons:
            res = self.parse_weaponhtml(wikia.page(subwiki, weapon[0]).html(), weapon[0], weapon[1].split("(")[0])
            rweps.append(res)
        self.charweap = rweps

    def get_highest_stats(self):
        if self.charweap is None:
            self.enumerate_weapons()
        stripwep = strip_weapon(self.charweap)
        topphy = sorted(stripwep, key=operator.itemgetter(2), reverse=True)[:4]
        topart = sorted(stripwep, key=operator.itemgetter(3), reverse=True)[:4]
        for idx, i in enumerate(topphy):
            # noinspection PyTypeChecker
            topphy[idx] = parse_strips(i)
        for idx, i in enumerate(topart):
            # noinspection PyTypeChecker
            topart[idx] = parse_strips(i)
        return topphy[:4], topart[:4]

    def construct_weapons(self):
        if self.charweap is None:
            self.enumerate_weapons()
        listpages = [[[self.pager.title, self.pager.url.replace(" ", "%20")],  # Fix spaces.
                      charformat(self.chardata['chara']),
                      f"[0/{len(self.chardata['weps'])+1}] "
                      f"[To View the weapons, Click the Arrows at the bottom!] ~=~=~=~=~=~=~=~=~=~=~=~=",
                      self.chardata['chara']['Avy-Awakened']]]

        counter = 1
        for i in self.charweap:
            counter += 1
            listpages.append([[i['name'][0], None],
                              codeformat(i),
                              f"{counter}/{len(self.chardata['weps'])+1} "
                              f"~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=",
                              i['image']])
        return listpages

    def parse_characterhtml(self):
        soup = BeautifulSoup(self.pager.html(), "lxml")
        tables = soup.find("article").find_all("table")
        weapons_table = None
        mirrage_table = None
        for table in tables:
            if table.find("tr").find("th").text.lower() == "weapons":
                weapons_table = table
            if table.find("tr").find("th").text.lower() == "mirrages":
                mirrage_table = table
                break
        weps1 = []
        weps2 = []
        for i in weapons_table.find_all("th", {"class": "weaponName"}):
            weps1.append(i("a")[0].get("title"))
        for i in weapons_table.find_all("th", {"class": "arteName"}):
            weps2.append(i.text)
        weps = list(zip(weps1, weps2))
        # Video Preview
        # 【テイルズ オブ ザ レイズ】
        artes = []
        for a in mirrage_table("tr")[1:]:
            for b in a("th"):
                if b.text:
                    if "arte enhancement" in b.text.lower():
                        pass
                    elif "video preview" in b.text.lower():
                        pass
                    elif "テイルズ" in b.text.lower():
                        pass
                    else:
                        artes.append(b.text)
        artes = tuple(Utils.grouper(2, artes, None))
        asidelist = soup('aside')[0]('div')
        charaside = {"name-full": soup('aside')[0].find('h2').text,
                     "name-jp": ["Field not found", ""],
                     "weapon": "Not Given"}
        plist = []
        for i in asidelist:
            classattrs = i.attrs.get('class')
            if 'pi-image-collection-tab-content' in classattrs:
                dataimg = i.find('a').find('img')
                charaside[f"Avy-{dataimg.attrs.get('alt').split(' - ')[0]}"] = i.find('a').attrs.get('href')
            elif 'pi-item' in classattrs:
                tagpair = i.text.replace('\n', '|')[1:][:-1].split("|")
                if tagpair[1] == '':
                    tagpair[1] = " ".join(i.find('img').attrs.get('alt').split(" ")[1:]).replace("sync-",
                                                                                                 "").lstrip().rstrip()
                plist.append(tagpair)
        plindexs = [x[0].lower() for x in plist]
        for a in plist:
            if a[0].lower() == "localized name":
                charaside["name-full"] = textwrap.wrap(a[1], width=20)
                charaside["name-full"].extend([''] * (2 - len(charaside["name-full"])))
            elif a[0].lower() == "kanji" and "japanese name" in plindexs:
                charaside["name-jp"] = textwrap.wrap(a[1].replace("・", ". "), width=16, break_long_words=False)
                charaside["name-jp"][0] = charaside["name-jp"][0].replace(".", "・")
                charaside["name-jp"].extend([''] * (2 - len(charaside["name-jp"])))
                charaside["name-jp"][1] = charaside["name-jp"][1].replace(".", "・")
                if charaside["name-jp"][1]:
                    charaside["name-jp"][1] = f'[{charaside["name-jp"][1]}]'
                charaside["name-jp"][0] = f'[{charaside["name-jp"][0]}]'
            elif a[0].lower() == "japanese name" and "kanji" not in plindexs:
                charaside["name-jp"] = textwrap.wrap(a[1].replace("・", ". "), width=16, break_long_words=False)
                charaside["name-jp"][0] = charaside["name-jp"][0].replace(".", "・")
                charaside["name-jp"].extend([''] * (2 - len(charaside["name-jp"])))
                charaside["name-jp"][1] = charaside["name-jp"][1].replace(".", "・")
                if charaside["name-jp"][1]:
                    charaside["name-jp"][1] = f'[{charaside["name-jp"][1]}]'
                charaside["name-jp"][0] = f'[{charaside["name-jp"][0]}]'
            elif a[0].lower() == "gender":
                rgend = get_gender(a[1], self.pager)
                charaside["gender"] = rgend[0]
                charaside["genderplus"] = rgend[1]
            elif a[0].lower() == "anima sync":
                charaside["anima"] = a[1]
            elif a[0].lower() == "source":
                charaside["game"] = a[1]
                charaside["game"] = textwrap.wrap(charaside["game"], width=23)
                charaside["game"].extend([''] * (2 - len(charaside["game"])))
            elif "en voice" in a[0].lower():
                pass
            else:
                charaside[a[0].lower().replace(" ", "-")] = a[1]
        if isinstance(charaside["name-full"], str):
            charaside["name-full"] = textwrap.wrap(charaside["name-full"], width=20)
            charaside["name-full"].extend([''] * (2 - len(charaside["name-full"])))
        self.chardata = {'weps': weps, 'arte': artes, 'chara': charaside}


def parse_strips(name_list):
    return f"Name: {name_list[0][0]}\n" \
           f"Arte: {name_list[0][1]}\n" \
           f"Lv:   {str(name_list[1]).ljust(3,' ')}\n" \
           f"Phy:  {str(name_list[2]).ljust(3,' ')}\n" \
           f"Arte: {str(name_list[3]).ljust(3,' ')}\n" \
           f"CC:   {str(name_list[4]).ljust(3,' ')}"


def quicken_skills(skills_list: list):
    rskills = []
    for skill in skills_list:
        askill = skill
        skill = skill.lower()
        sksplit = skill.split(" ")
        for i in sksplit:
            if '%' in i:
                percent = i
                break
        for i in sksplit:
            if '.' in i:
                dot = i
                break
        if "mirrage gauge" in skill:
            rskills.append(f"MG +{percent}")
        elif "recover" in skill and "hp recovery" not in skill:
            rskills.append(f"HP Vampirism {percent}")
        elif "hp recovery" in skill:
            if "boost" in skill:
                rskills.append(f"HP Recovery +{percent}")
            elif "caster" in skill:
                rskills.append(f"Self Heal +{percent}")
        elif "damage" in skill:
            rskills.append(f"DMG +{percent}")
        elif "stagger" in skill:
            if "single shot" in skill:
                rskills.append(skill)
            else:
                rskills.append(f"Stagger Time -{dot}s")
        elif "rush distance" in skill:
            rskills.append(f"Rush distance +{percent}")
        elif "invincibility after use" in skill:
            rskills.append(f"{dot}s I-frame after use")
        elif "cc" in skill:
            for i in sksplit:
                try:
                    p = int(i)
                    break
                except ValueError:
                    pass
            rskills.append(f"CC -{percent}")
        elif "area of effect" in skill:
            rskills.append(f"AOE +{percent}")
        elif "iron stance" in skill:
            for i in sksplit:
                if '+' in i:
                    p = i
                    break
            rskills.append(f"IS BREAK {p}")
        elif "arte effect" in skill and "allies" in skill:
            rskills.append(f"Party-wide effect")
        elif "increase knock" in skill:
            rskills.append(f"Knockback power +{percent}")
        elif "casting speed" in skill:
            for i in sksplit:
                if '%' in i:
                    p = i
                    break
            rskills.append(f"Cast Time -{percent}")
        elif "chance" in skill and "" in skill:
            if "casting" in skill:
                rskills.append(f"{percent} Quick casting chance")
            elif "attack" in skill and "double" in skill:
                rskills.append(f"{percent} of 2x Attack Power")
            elif "fatigue" in skill:
                rskills.append(f"Can Cause <Fatigue> Ailment")
            elif "paralysis" in skill:
                rskills.append(f"Can Cause <Paralysis> Ailment")
            elif "tired" in skill:
                rskills.append(f"Can Cause <Tired> Ailment")
            else:
                rskills.append(skill)
        elif "ailment" in skill:
            if "odds" in skill:
                rskills.append(f"Ailment chance +{percent}")
            elif "strength" in skill:
                rskills.append(f"Ailment Effect +{percent}")
        elif "float" in skill:
            if "speed" in skill:
                rskills.append(f"Slows Enemy Descent")
            elif "power" in skill:
                rskills.append(f"Float power {percent}")
        else:
            rskills.append(askill)
    return rskills


def strip_weapon(weapon_list):
    rlist = []
    for wep in weapon_list:
        lb = wep.get('lb')
        weplist = [wep.get('name')]
        data = [0, 0, 0, 0]
        for idx, a in enumerate(lb):
            if a[2] == '':
                break
            else:
                data = [int(a[1]),
                        int(a[2]),
                        int(a[3]),
                        int(a[4])]
        for a in data:
            weplist.append(a)
        sd = wep.get('sidedata')
        weplist.append(sd)
        rlist.append(weplist)
    return rlist


def fix_lb(lb: list):
    rlb = []
    cap = 6
    for i in lb:
        rlb.append(f"{i[1]:<5}")
        rlb.append(f"{i[2]:<5}")
        rlb.append(f"{i[3]:<5}")
        rlb.append(f"{i[4]:<4}")
    return rlb


def codeformat(weapondata: dict):
    name, arte = weapondata.get('name')
    artetype = weapondata.get('sidedata')[0][1]
    element = []
    for i in weapondata.get('sidedata'):
        if i[0] == '-element-':
            element.append(f"{i[1]}")
        elif i[0] == '-rarity-':
            rarity = i[1]
    if not element:
        element = "Null"
    else:
        element = " & ".join(element)
    rarity = f"{int(rarity)*'★'}"
    packed = [name, arte, artetype, element, rarity, *fix_lb(weapondata.get("lb")),
              *quicken_skills(weapondata.get("skills"))]
    return formatting.format(*packed)


def charformat(chara: dict):
    return charformatstring.format(**chara)


def get_gender(original_gender, pager):
    if pager.title.lower() == "rita":
        return f"[{original_gender}]", "[GeniusMage]"
    elif pager.title.lower() == "milla":
        return f"[{original_gender}]", "[Daniel]"
    elif pager.title.lower() == "ludger":
        return f"[{original_gender}]", "[Ryu]"
    elif pager.title.lower() == "sync":
        return f"[{original_gender}]", "[Sink]"
    elif pager.title.lower() == "estelle":
        return f"[{original_gender}]", "[Shinykirby]"
    else:
        return f"[{original_gender}]", ""


def cacheResults():
    """
    On calling this method
    :return:
    """
