#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os

import Dconst
from Utilities import Utils


async def saucenao(imageurl):
    string = f'https://saucenao.com/search.php?db=999&api_key={Dconst.keys[0]}' \
             f'&output_type=2&testmode=1&numres=1&url={imageurl}'
    aj = await Utils.asyncrequest('POST', string, True)
    status = aj['header']['index']['0']['status']
    if status == 429:
        return [2, "Too many Requests!"]
    elif status > 0:
        return [2, f"Server Error, Try again. Status: {status}"]
    elif status < 0:
        return [2, f"Client Error, Try again. Status: {status}"]
    header = aj['results'][0]['header']
    data = aj['results'][0]['data']
    try:
        similarity = header['similarity']
        thumb = header['thumbnail']
        try:
            saucesite = str(header['index_name']).split(' - ')[0].split(': ')[1]
        except Exception as e:
            saucesite = os.path.splitext(header['index_name'][0])
        try:
            sauceurl = data['ext_urls'][0] or "No URL? THIS IS ~~SPARTA!~~ PYTHON!"
        except KeyError as e:
            saucedump(e, data, header)
            sauceurl = "No URL? THIS IS ~~SPARTA!~~ PYTHON!"
        for attempt_correct_name in ['title', 'eng_name', 'source']:
            title = data.get(attempt_correct_name, 'No title... It Seems!')
        # Should NEVER happen!
        # noinspection PyUnboundLocalVariable
        saucedata = [title, thumb, similarity, saucesite, sauceurl]
        return saucedata
    except Exception as e:
        saucedump(e, data, header)
        return [2, f'Failed to sort Data! Error: {e}']


def saucedump(exception, data, header):
    logging.error("/------ Error in Saucenao API!-\\")
    logging.exception(exception, exc_info=True)
    logging.info("|---------- DATA DUMP ---------- |")
    logging.info(data)
    logging.info(header)
    logging.error("\\------------------------------/")
