# Yuribot
Licensing:  
This is licensed under the Apache license. Version 2.0 to be exact.  
Although Talesbot(Yuribot) is released as a open source, I don't want random clones popping up trying to "be better" than Yuribot. 

Please note that this repo may randomly become private at anytime.  
If it does, The Apache license will only hold true to the copy that you have.

As of 1.4, the bot will silently update to include more features.
so there won't be any definitive dates when the next update will be truely out.

Current Release Version:  
Version: 1.4.0-RR [[Pride Kakumei](https://www.youtube.com/watch?v=T1w9zGAZ1qU)]  
Development:  
Version: 1.4.X-RR [[Asada Shinon](https://i.imgur.com/z8wOCCY.png)]