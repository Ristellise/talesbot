"""
Command line interface to send messages.
"""
import requests
import Dconst
import json

import discord,aioconsole
did = input('Channel ID >:')
did = int(did)
print("Ready.")
while True:
    message = input(">:")
    ab = requests.post(f'https://discordapp.com/api/channels/{did}/messages',
                       headers={'User-Agent': 'TalesBotV2(http://google.com, v1.0)',
                                "Content-Type": "application/json",
                                "Authorization": f"Bot {Dconst.bottoken}"},
                       data=json.dumps({'content': str(message)}))
