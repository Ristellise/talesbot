#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import logging
import sys
import traceback

import coloredlogs

# We set basiclogging first so that the other ones follow this.
logging.basicConfig(level=logging.INFO,
                    handlers=[logging.FileHandler("Infolog.txt", 'w', 'utf-8'),
                              logging.StreamHandler(stream=sys.stdout)])
logging.info("Loaded Logging basicConfig")
exceptioncatch = logging.getLogger('ExceptionCatcher')
mainlogger = logging.getLogger('Main')
import Dconst

fstyle = {'asctime': {'color': 'green'}, 'hostname': {'color': 'magenta'},
          'levelname': {'color': 'black', 'bold': True}, 'name': {'color': 'cyan'},
          'programname': {'color': 'cyan'}}
logging.info("Attempting to Install Colors...")
if not sys.platform == 'win32':
    coloredlogs.install(level='INFO', fmt='%(asctime)s|%(name)s|%(levelname)s:%(message)s',
                        field_styles=fstyle)
elif Dconst.windowsignore is True:
    coloredlogs.install(level='INFO', fmt='%(asctime)s|%(name)s|%(levelname)s:%(message)s',
                        field_styles=fstyle)
    mainlogger.info("Bypassing Windows Exclusions...")
else:
    mainlogger.info("Disabled Colors: Pycharm Sets all the colors to a default color.")
mainlogger.info("Importing...")
import os, sys, signal
import re
import time
import discord
from discord.ext import commands
from Utilities import serverroutines, Utils, discordembedpages, rethinkdbaccessorv2, guildjsonutilites
from botmodules import *
import datetime

if 'vcedition.py' in os.listdir(os.path.join(os.getcwd(), 'botsecretstuff2')):
    # Run VCEdition Testing build
    mainlogger.info("Using Running Test build...")
    from botsecretstuff2 import vcedition

    Dconst.prefix = vcedition.prefix
    Dconst.testb = True
# Globals

start = time.time()

bot = commands.AutoShardedBot(command_prefix=Utils.serverargs,
                              self_bot=False, pm_help=True, case_insensitive=True)


@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        return
    elif isinstance(error, commands.CommandNotFound):
        return
    else:
        exceptioncatch.error('---------------------------------------')
        a = [z.split('    ') for z in
             [traceline.replace('\n', '') for traceline in
              traceback.format_exception(type(error), error, error.__traceback__)]]
        for y in a:
            for z in y:
                if 'The above exception' in z:
                    exceptioncatch.error('---------------------------------------')
                    return
                exceptioncatch.error(z.replace('  ', ''))
        exceptioncatch.error('---------------------------------------')


@bot.before_invoke
async def doratelimit(ctx):
    Utils.bypassablecooldownmapping.get_bucket(ctx).update_rate_limit()


async def bgchecktimeouts():
    await bot.wait_until_ready()
    while not bot.is_closed():
        await asyncio.sleep(1)
        timeouts = discordembedpages.embedpages.checktimeouts()
        if len(timeouts) == 1:
            pass
        else:
            for atimeout in timeouts[:-1]:
                try:
                    await atimeout.edit(embed=timeouts[-1])
                except discord.NotFound:
                    pass


async def eventschecker():
    await bot.wait_until_ready()
    while not bot.is_closed():
        tnow = datetime.datetime.utcnow().utctimetuple().tm_yday
        if tnow == datetime.datetime(2018, 9, 21, 0, 0, 0, 0).utctimetuple().tm_yday \
                and Dconst.september is False:
            print("September")
            if testb:
                chan = bot.get_channel(492735840188104724)
            else:
                chan = bot.get_channel(276035355369799680)
            await chan.send("Hey. Do you know what Day is it?")
            await asyncio.sleep(2)
            await chan.send("No? Well let me refresh your memory.")
            f = open(random.choice(["september.mp4", "september2.mp4", "september3.mp4"]), "rb")
            await chan.send(file=discord.File(f, filename="september.mp4"))
            await chan.send("----------------\n"
                            "Yes it is the 21st of September.\n<https://www.youtube.com/watch?v=Gs069dndIYk>\n"
                            "Video Sources: <https://twitter.com/electrolemon>")
            f.close()
            Dconst.september = True
        if tnow == 265:
            Dconst.september = False

        # Reduce counting
        await asyncio.sleep(1)


async def randomisegame():
    await bot.wait_until_ready()
    while not bot.is_closed():
        await asyncio.sleep(7200)
        await bot.change_presence(activity=Dconst.getactivity())


@bot.event
async def on_reaction_add(reaction, user):
    if user.id != bot.user.id:
        embedable = discordembedpages.embedpages.check(str(reaction.message.id), str(reaction.emoji), user)
        if embedable is True:
            # delete message
            await Utils.trydelmsg(reaction.message)
        elif embedable is not False:
            try:
                await reaction.message.remove_reaction(reaction.emoji, user)
            except discord.Forbidden:
                pass
            except discord.NotFound:
                pass
            await reaction.message.edit(embed=embedable)

    if str(reaction) == '❎':
        if reaction.message.author.id == bot.user.id and user.id != bot.user.id:
            if str(reaction.message.embeds[0].description) == f'Requested by: {user.id}':
                await Utils.trydelmsg(reaction.message)


@bot.event
async def on_guild_join(guild):
    # The last is a testing server.
    if guild.id in [417299270605996043, 345832994050932767]:
        pass
    else:
        guilder = guild
        botcount = 0
        usercount = 0
        for y in guild.members:
            if y.bot is True:
                botcount += 1
            else:
                usercount += 1
        mainlogger.info(f"BC: {botcount} UC: {usercount}")
        if botcount >= usercount:
            logging.warning(f"This Server: {guilder} with the ID of {guilder.id}, has been collecting bots.\n"
                            f"Owner: {guilder.owner} ")
            await guild.leave()
        else:
            mainlogger.info(f"This Server: {guilder} with the ID of {guilder.id}, added The bot.\n"
                            f"Bot list filter was passed and it's Safe.")


@bot.event
async def on_ready():
    mainlogger.info('/---------Bot is Ready!-------------')
    mainlogger.info('|Name: ' + bot.user.name)
    mainlogger.info('|ID: ' + str(bot.user.id))
    mainlogger.info(f"|Version: {'| '.join(Dconst.version)}")
    mainlogger.info(f"|Shards: {bot.shard_count}")
    mainlogger.info('\\------------------------------------')
    for emoteserver in bot.get_guild(345832994050932767).emojis:
        Dconst.elist.append([str(emoteserver), emoteserver.name])
    activity = Dconst.getactivity()
    await bot.change_presence(activity=activity)
    if not discord.opus.is_loaded():
        mainlogger.info("Opus not loaded!")
        try:
            mainlogger.info("Attempting to load internal opus...")
            discord.opus.load_opus(os.path.join(os.getcwd(), 'libopus-0.x86.dll'))
            mainlogger.info('OK, Used internal opus library as fallback.')
        except Exception as on_ready_error:
            mainlogger.critical(on_ready_error)
            mainlogger.critical('failed! THIS IS NOT GOOD!')


@bot.event
async def on_member_join(member: discord.Member):
    jsonf = guildjsonutilites.get_instance(member.guild)
    if jsonf.get_config("welcome"):

        channel = jsonf.get_config('welcome-channel')
        welcomemessage = jsonf.get_config('welcome-message')
        if channel is not None and welcomemessage is not None:
            chan = member.guild.get_channel(channel)
            await chan.send(welcomemessage.replace("%user%", member.display_name))
    else:
        pass


def filteremoji(message: str):
    return list(filter(None, re.findall(r":(.*?):", message)))


@bot.event
async def on_message(message: discord.Message):
    if str(message.author.id) in Dconst.globalbanlist:
        return
    if isinstance(message.channel, discord.DMChannel):
        return await bot.process_commands(message)
    elif isinstance(message.channel, discord.TextChannel):
        GuildJsonInstance = guildjsonutilites.get_instance(message.guild)
        if GuildJsonInstance.get_config("emoji"):
            filteredemojis = filteremoji(message.content)
            if len(filteredemojis) > 0:
                emojimessage = ""
                for emoji in Dconst.elist:
                    if str(emoji[1]) == str(emoji):
                        message += f"{emoji[0]} "
                await message.channel.send(emojimessage[0])
    # Exp.
    await bot.process_commands(message)
    ctx = await bot.get_context(message)
    if not ctx.valid and not ctx.author.bot and rethinkdbaccessorv2.connected and ctx.guild is not None:
        await reg.reg.check(message, bot)


# ---------------------------------------------------
module_list = [cashconvert.Currency(bot), emotes.emotes(bot),
               miscellaneouscommands.MiscellaneousCommands(bot), MusicModuleDE.music(bot), quotes.quoteshook(bot),
               reg.reg(bot), search.search(bot), tales.tales_commands(bot),
               Core.Coreo(bot), Core.Moderation(bot), users.users(bot), weebsh.weebsh(bot), gald.gald(bot)]
cogman = logging.getLogger('Cog Manager')
for a_module in module_list:
    try:
        bot.add_cog(a_module)
        cogman.info(f"Added cog: {str(a_module.__class__.__name__)}")
    except Exception as e:
        cogman.info(e)
        cogman.info(f"failed to add cog: {str(a_module.__class__.__name__)}")


def b(modulestring, searchclassstring):
    for i in inspect.getmembers(sys.modules[modulestring], inspect.isclass):
        if i[0] == searchclassstring:
            return i[1]


def reloadall():
    for a_module in module_list:
        mainlogger.info(f"Unloading Extension: {a_module.__class__.__name__}")
        bot.unload_extension(a_module.__class__.__name__)
        mainlogger.info(f"Reloading: {a_module.__module__}")
        importlib.reload(sys.modules[a_module.__module__])
        mainlogger.info(f"Loading Extension: {a_module.__class__.__name__}")
        bot.load_extension(f"{sys.modules[a_module.__module__].__name__}.{a_module.__class__.__name__}")
    mainlogger.info("All done!")


def signal_handler(signal, frame):
    print(f'Caught: Signal ID: {signal}. Logging out')
    bot.logout()
    print("Done. Terminating Process... Now. Stacktrace:")
    traceback.print_stack(frame)
    mainlogger.info('Goodbye! o/')
    sys.exit(0)


def segfault(signal, frame):
    print("I'm often unseen, but will always appear given enough time,")
    print("I end things. What am I?")
    time.sleep(1)
    print("Hmm..")
    print("You're a-")
    time.sleep(1)
    print("========================== THIS IS NO JOKE =============================")
    print("If your are reading this,")
    print("It means that the segfault has been caught successfully! Which is nice")
    print("========================== STACKTRACE IS AS FOLLOWS --------------------")
    traceback.print_stack(frame)
    print("========================================================================")
    print("Attempting to restart the bot. Hold tight!")
    os.execl(sys.executable, "python3", "MainBot.py")


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)
signal.signal(signal.SIGSEGV, segfault)
bot.loop.create_task(bgchecktimeouts())
bot.loop.create_task(randomisegame())
bot.loop.create_task(eventschecker())
if 'vcedition.py' in os.listdir(os.path.join(os.getcwd(), 'botsecretstuff2')):
    @bot.command()
    async def rewrite(ctx):
        await ctx.send("Hey Spicy World! This is Shinon/Rita. I know it's been a while... "
                       "But I realised I have to refractor the largest backend for this bot.")
        await ctx.send("The JSON backend stores things such as quotes, emotes, configs and friend ID's.")
        await ctx.send("For now, the quotes are now using the new backend. And with the new backend, I can do things "
                       "like this:")


    import importlib, sys

    Dconst.version = vcedition.version
    bot.run(vcedition.token)



else:
    # Run official
    bot.run(Dconst.bottoken)
