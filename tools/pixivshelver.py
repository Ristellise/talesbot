import json
import shelve
import ijson

"""
Converts json data to a shelve object.
"""


def converttoshelve(file, shelvefile):
    shelve.open(shelvefile, )
    with open(file, "r") as f:
        items = ijson.items(f, "item")
        while True:
            try:
                item = next(items)
            except StopIteration:
                break
            item.get()
