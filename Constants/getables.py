"""
Mee6's Experience formula
"""
import math


def baseround(x, base=5):
    return int(base * round(float(x) / base))


Mee6 = []
for i in range(5000):
    val = baseround(-0.00000000000001 * i ** 4 + 1.6667 * i ** 3 + 22.5 * i ** 2 + 75.833 * i - 0.000003)
    Mee6.append(val)
Gald = []