# -- START YTDL STUFF ---
# ytdl base. (ALL YOUR BASES ARE BELONG TO US)
import youtube_dl
import os
ytdl_base = {
    'format': 'bestaudio/best',
    'outtmpl':os.path.join("audiofiles", 'bot-%(title)s.%(ext)s'),
    'restrictfilenames': True,
    'nocheckcertificate': True,
    'ignoreerrors': True,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'}
ytdl_no_playlist = {
    'noplaylist': True,
}
ytdl_playlist = {
    # extract flat. we want only info.
    # not the raw stuff. speeds up a lot.
    'extract_flat': True,
    'in_playlist': True}
ytdl_playlist_search = {
    # extract flat. we want only info.
    # not the raw stuff. speeds up a lot.
    'extract_flat': False,
    'in_playlist': True}
ytdl_usearia = {
    'external_downloader': 'aria2c',
    'external_downloader_args': ['-q', '--min-split-size', '1M', '--max-connection-per-server', '16', '--split', '16',
                                 '--max-concurrent-downloads', '16']}
ytdl_random = {
    'playlistrandom': True}
ytdl_streaming = {
    'nopart': True
}
ytdl_debug = {
    'logtostderr': True,
    'quiet': False,
    'no_warnings': False,
}


# Dynamic YTDL this helps with cleaning of dconst's
def dynytdl(*args):
    """
    :param args: playlist/noplaylist || aria || random(req playlist)
    :return: youtube_dl.YoutubeDL(base)
    """
    base = ytdl_base
    # do we use aria?
    if 'aria' in args:
        base = {**base, **ytdl_usearia}
    # playlist or no playlist?
    if 'noplaylist' in args:
        base = {**base, **ytdl_no_playlist}
    elif 'playlist' in args:
        base = {**base, **ytdl_playlist}
    elif 'search' in args:
        base = {**base, **ytdl_playlist_search}
    # random only works for playlists.
    if 'playlist' in args and 'random' in args:
        base = {**base, **ytdl_random}
    if 'streaming' in args:
        base = {**base, **ytdl_streaming}
    base = {**base, **ytdl_debug}
    return youtube_dl.YoutubeDL(base)

# -- END YTDL STUFF ---
