#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import psutil, random
from discord.ext import commands
from Constants import ytdlconstants
from botsecretstuff2 import botsecrets
import discord
import datetime
from dateutil import tz
import asyncio

# -----LOCKDOWN----
lockdown = False
# ----USE WITH CAUTION----
# WITH GREAT POWER COMES GREAT RESPONSIBILITY.

VolumeLevel = 0.5
confirmflush = False
# Bits do not edit these.
# used for communication cross classes
weather = 60
enabledebug = 1
elist = []
prefix = ['tales!', 'Tales!']
aprilfools = False
gachalimit = False
festivals = {'getseasonal': None,
             'Awa Odori': False,
             'tales of festival': False,
             'Rays': [''],
             }

version = ['1.4.X - RollingRERelease', """ALICIZATION-DAMAS"""]
mail = botsecrets.mail
__dir__ = ['']

globalbanlist = []
# bot colors
limegreen = (50, 205, 50)
BirthdayWishes = ["Happy Birthday {}! May your days be fruitful and your nights *tormented!*",
                  "Wait what? It's your birthday? Happy Birthday then {}!",
                  "You're old enough to start learning about bazongas! Congrats {}! you could do `so!emote get "
                  "bazongas` for more info.",
                  "You Earned 1 Free massage coupons! To Use them, Contact **Leia Rolando** at the Following hotline:\n"
                  "0118-999-881-999-119-7253\nOn Second thoughts... don't do that.",
                  ]
keylist = "skips-reqired: Default (3) please do set this if you want skips to use different variables.\n" \
          "stops-reqired: Default (0) Number of Users Voting to Stop the Music Playback\n" \
          "disable-song-info: Default (Off) Set this to True to disable song info when a new song is playing.\n" \
          "do-not-delete-messages: Default (On)" \
          "use-aria: Default (0) Set to 1 to enable the use of aria2c to download videos. Helps for long videos.\n" \
          "So far, Aria2c with tons of voice testing, seems to very stable." \
          "hostility: Default (0) Set to `1` to remove the `-sudo` requirment.\n" \
          "global-emoji: Default(0) Set to `1` to enables emojis listed under `!emotelist`\n" \
          "enable-global-nickchanges: Default (0) Set to `1` to enable Global Nicknames from Joshwoo70.¹\n" \
          "emote-role-id: Set to enable a specific role to use emotes."
timeout = 20
# Festival Stuff (Christmas)
"""
christmas = [['http://78.media.tumblr.com/f1a7bfcc10fef3a1394de35e08773c8c/tumblr_nzxwv7iEPZ1typ47go2_1280.png',
              'https://nera-loka14.deviantart.com/art/Tales-of-Zestiria-Christmas-Edna-580256999'],
             ['http://78.media.tumblr.com/3952b5e456356d24de75c61a07b07acf/tumblr_orokd9heXA1u2lyiwo1_1280.png',
              'Tales of Link'],
             ['http://78.media.tumblr.com/2f75481444edb12b4e73599bf33ebd9c/tumblr_nsaosynzdr1u2lyiwo1_1280.png',
              'http://tales-of-transparent.tumblr.com/post/125935409387'],
             ['https://orig00.deviantart.net/cd48/f/2012/033/b/8/christmass_with_symphonia_girls___and_genis_by_trinity'
              'inyang-d4ohb7t.jpg',
              'https://trinityinyang.deviantart.com/art/Christmas-with-Symphonia-girls-and-Genis-282983177'],
             ['https://i.imgur.com/fKvu5lE.jpg', 'https://www.pixiv.net/member.php?id=491638'],
             ['https://img00.deviantart.net/4901/i/2010/358/9/3/tales_of_graces_merry_xmas_by_a745-d35ktch.jpg',
              'https://a745.deviantart.com/art/Tales-of-Graces-Merry-Xmas-190767761'],
             ['https://orig00.deviantart.net/89a0/f/2016/006/d/5/tales_of_zestiria_600_1851067_by_yugioh5dsduelist-d9mz'
              'y9s.jpg',
              'https://yugioh5dsduelist.deviantart.com/art/Merry-Christmas-from-Rose-and-Dezel-582824512'],
             ['https://i.imgur.com/KAQbSod.jpg',
              'http://www.pixiv.net/member.php?id=776924'],
             ['https://i.pximg.net/img-original/img/2010/12/24/17/51/22/15401205_p0.jpg',
              'https://www.pixiv.net/member_illust.php?mode=medium&illust_id=15401205'],
             ['https://i.imgur.com/VJBCGH0.png',
              'https://www.pixiv.net/member_illust.php?mode=medium&illust_id=60537203'],
             ['https://i.imgur.com/1kRIDyC.png',
              'https://www.pixiv.net/member.php?id=16889101'],
             ['https://i.imgur.com/olpsI07.jpg',
              'https://www.pixiv.net/member.php?id=14052091'],
             ['https://i.imgur.com/GpPZFIy.png',
              'https://www.pixiv.net/member.php?id=3204230'],
             ['https://i.pximg.net/img-original/img/2016/12/17/21/52/17/60418198_p0.png',
              'https://www.pixiv.net/member.php?id=3687143'],
             ['https://i.imgur.com/qxBC8it.jpg',
              'Tales of the Rays'],
             ['https://i.imgur.com/Dr0JLoU.jpg',
              'Tales of the Rays'],
             ['https://i.imgur.com/Gkv4mGh.jpg',
              'https://www.pixiv.net/member_illust.php?mode=medium&illust_id=60570976'],
             ['https://i.imgur.com/yOswHs4.jpg',
              'https://www.pixiv.net/member.php?id=4538400'],
             ['https://i.imgur.com/Rfji0HD.png',
              'https://www.pixiv.net/member_illust.php?id=4484082'],
             ['https://i.imgur.com/B6NpqUG.png',
              'https://www.pixiv.net/member.php?id=2793633'],
             ['https://i.imgur.com/bTqLrvs.jpg',
              'https://www.pixiv.net/member.php?id=13419475'],
             ['https://i.imgur.com/3YyD7sX.jpg',
              'https://www.pixiv.net/member.php?id=607406'],
             ['https://i.imgur.com/auu7VhO.jpg',
              'https://www.pixiv.net/member.php?id=404335'],
             ['https://i.imgur.com/E5PqWkC.png',
              'https://www.pixiv.net/member.php?id=19048505'],
             ['https://i.imgur.com/NnEi31m.jpg',
              'https://www.pixiv.net/member.php?id=12739753'],
             ['https://i.imgur.com/Xv7b9vh.png',
              'https://www.pixiv.net/member.php?id=16967595']
             ]
"""
dynytdl = ytdlconstants.dynytdl
# --- Caching
# Why caching? Because If i don't do so.. I wind up with ~ 1000 _real_extract objects.
cachedynamicytdl = [dynytdl('aria', 'noplaylist'), dynytdl('playlist'), dynytdl('playlist', 'random'),
                    dynytdl('noplaylist')]


assets = ['Wagnaria',
          '\"Good evening, I\'m Santa Claus!\"',
          'as Zura but wait, it\'s Pine tree!',
          'Just do it [✓]',
          'Ringing a bell',
          'Tales of... ASTERIA?!',
          'Ni no Kuni II: Revenant Kingdom',
          'Dark Souls',
          'Dark Souls II',
          'Dark Souls III',
          'Tales of Zestiria',
          'Tales of Symphonia',
          'Tales of Berseria'
          'Protecc with Tales of Graces [ƒ]',
          'Final Act on a Synth',
          'with SOMEONE ELSE',
          'tales!help to get some actual help',
          'Mitsukete Your dreams...',
          'Tales of Vesperia: Definitive Edition [Beta]',
          'Jouney\'s end',
          'relaxing at a mountain village',
          'at a beloved hometown',
          'with daring swords with Jude',
          'ACROSS by 水樹奈々',
          'Believe in yourself!',
          'Tales of the Tempest OST',
          'to know the way of the Katz',
          'with snow lights.',
          'with Rheairds in the sky...',
          'looking up to the star in the sky',
          'Gintama.: Slip Arc',
          'Farewell, Gintama...',
          'Shaking Inmai-chan',
          "Chu! Chu! Yeah!",
          "Dabbing away the pain",
          "with !ass's"]
assetsshinon = ["ALfehim Online",
                "Gun Gale Online",
                "ALO", "GGO",
                "to hit that 100 metre mark",
                "RL",
                "SAO: Memory Defrag",
                "...",
                'Sword Art Online Re: Hollow Fragment',
                'Sword Art Online: Fatal Bullet',
                "New Aincrad",
                "Sword Art Online: Integral Factor", ]

getgald = [["**{name}**: You've Claimed your daily Gald of {daily} for the day!\n"
            "Come back in {time} to claim it again.", "Sorey"],
           ["Here. Take it. It's your daily gald of {daily}. come back in {time} for another round.", "Leon"],
           ["... So we need to pack this... and that...\n"
            "Oh hey **{name}**! Here's your {daily} gald for the day.\n"
            "And we have to pack this as well...", "Ix"],
           ['\"Hey, Rita! What\'s an old man gotta do to get all this gald?\"\n'
            '\"Ugh. You are such a creep-o... Oh hey there **{name}**. Take here\'s your {daily} gald for the day.\"\n'
            '\"Soo when can I get those gald again?\"\n'
            '\"Maybe {time} from now.\"',
            "Rita & Raven"],
           ]

toolittlegaldtime = [["**{name}** your back that soon for more gald? We aren't charity you know...\n"
                      "Come back in about {time}", "Velvet"],
                     ["**{}**"]]

loadingmsg = ["Reticulating Splines...",
              "Hugging Estelle...",
              "Watching Tales of Vesperia: ~The First Strike~",
              "Reticulating splines...",
              "Generating witty dialog...",
              "Swapping time and space...",
              "Spinning violently around the y-axis...",
              "Tokenizing real life...",
              "Bending the spoon...",
              "Filtering morale...",
              "We need a new fuse...",
              "Upgrading Windows...",
              "drafting with architects...",
              "Breeding bits...",
              "Making Terca Lumireis great again...",
              "Summoning Friends...",
              "Catching lost bits...",
              "Checking the gravitational constant in your locale...",
              "Waiting for a large software vendor in Singapore to take over the world...",
              "Preparing to take over the world...",
              "Testing your patience...",
              "```> make sandwich\n~ What? Make it yourself.\n> sudo make sandwich\n~ Okay```"
              "Waiting for the satellite to move into position",
              "Installing google Gnome on discord...",
              "Testing on Timmy...",
              "Reconfoobling energymotron...",
              "Counting backwards from Infinity...",
              "Embiggening Prototypes...",
              "Creating time-loop inversion field...",
              "Spinning the wheel of fortune...",
              "Loading the enchanted bunny...",
              "Computing chance of success...",
              "Looking for exact change...",
              "Adjusting flux capacitor...",
              "Unicorns are at the end of this road, I promise.",
              "Listening for the sound of one hand clapping...",
              "Keeping all the 1's and removing all the 0's...",
              "Putting the icing on the cake. The cake is not a lie...",
              "Cleaning off the cobwebs...",
              "Making sure all the i's have dots...",
              "Connecting Neurotoxin Storage Tank...",
              "Granting wishes...",
              "Spinning the hamster...",
              "Convincing AI not to turn evil...",
              "Constructing additional pylons...",
              "Roping some seaturtles...",
              "Locating Jebediah Kerman...",
              "Hello IT, have you tried turning it off and on again?",
              "Hello, IT... Have you tried forcing an unexpected reboot?",
              "Dividing by zero...",
              "Cracking military-grade encryption...",
              "Simulating traveling salesman...",
              "Proving P=NP...",
              "Entangling superstrings...",
              "Twiddling thumbs...",
              "Searching for plot device...",
              "Trying to sort in O(n)...",
              "Looking for sense of humour...",
              "Waiting for the Raven to refil his coffee...",
              "Hold on while we wrap up our git together...",
              "Reheating our coffee...",
              "converting a bug to a feature...",
              "Wating for our intern to quit vim...",
              "Winter is coming...",
              "Installing dependencies...",
              "Switching to the latest JS framework...",
              "Getting distracted by cat gifs...",
              "Finding someone to hold my beer",
              "Working on my side project...",
              "Ordering 1s and 0s...",
              "Updating dependencies...",
              "Consulting the manual...",
              "Loading funny message...",
              "Waiting for Rita to say all her titles...",
              "format C: ...",
              "Mining some bitcoins...",
              "Downloading more RAM...",
              "Initializing the initializer...",
              "Optimizing the optimizer...",
              "Running swag sticker detection...",
              "Pushing pixels...",
              "Building a wall...",
              "Updating the Updater...",
              "Downloading the Downloader...",
              "Debugging the Debugger...",
              "Reading Terms and Conditions...",
              "Loading Cow level...",
              "Deleting all your hidden Judith pics...",
              "Running with scissors...",
              "Discovering new ways of making you wait...",
              "Obfuscating quantum entaglement...",
              "Decompiling Tales of the Rays",
              "Buffering before the Raid...",
              "Loading VS - Blue Encount",
              "Enumerating Weapons... This might take a while!",
              "PINGING @[SOMEONE ELSE](https://youtu.be/s9Z9Ysc31AE)\n***DABS***",
              "Catching too much eye power...",
              "Flying to Tales of Eternia...",
              "Getting a Light Infection...",
              "Generating a Stairway Generation Algorithm...",
              "Generating Delta differance...",
              "Trolling the Rays community with *Sword Art Online*"
              ]


def get_loading():
    random.seed()
    return discord.Embed(title="Loading...", description=random.choice(loadingmsg),
                         color=0x36d7a5)


def getactivity():
    return discord.Game(name=random.choice(assets))


"""
Type: 0 - Playing
Type: 1 - Streaming (Requires a twitch.tv url)
Type: 2 - Listening to
Type: 3 - Watching
"""
ffbefopts = '-nostdin'
ffopts = '-vn -reconnect 1'
radio = {'symphony': "http://mediacorp-web.rastream.com/924fm", "listen.moe": "https://listen.moe/stream",
         "hotmixjp": "http://streaming.hotmixradio.fm/hotmixradio-japan-64.aac"}
windowsignore = False
# Private Variables
process = psutil.Process(os.getpid())
privatevariable = ['privatevariable', 'headers', 'wbfns', 'discogsclient', 'discogsua', 'process', 'token',
                   'wolketoken', 'botspwheaders', 'saucenao', 'keys', 'bottoken', 'vcedition', 'sys', 'platform']
userhide = ['']
keys = botsecrets.keylist
wolketoken = {'Authorization': f"Wolke {keys[2]}", 'User-Agent': f"TalesBot/{version[0]}"}
ocrtoken = botsecrets.ocr
bottoken = botsecrets.bottoken
september = True
levelup = [":confetti_ball: Grats **{userping}**! Your now level {level}!",
           "Congrats **{userping}**! You're now level {level}!\n"
           "And before you ask... I totally didn't steal that line from Meebo.",
           "Congrats on reaching level: {level}, **{userping}**!",
           "**{userping}** has leveled up his overray character to {level}!",
           "Whats this? **{userping}** is now {level}? Congrats!",
           "*Sets aside the chat\nBooming Voice*\n**{userping}** has leveled to level {level}!",
           "I can't believe that **{userping}** has leveled to {level}!",
           "Hold the chatter for now since **{userping}** has leveled up to {level}!",
           "time to celebrate **{userping}**! Grats on reaching: {level}!",
           "**{userping}**! FRESH LEVEL {level}'s!",
           "**{userping}**, stop it, get some level {level}.",
           "PIKOHA- Oh **{userping}** has leveled to {level}!\n"
           "***PIKOHAN!***",
           "*Tonight's Theme is...* level {level} for **{userping}**."]

discordprecoloredembeds = {'warn': discord.Embed(color=discord.Color.red()),
                           'fatal': discord.Embed(color=discord.Color.dark_red()),
                           'default': discord.Embed(color=discord.Color.green())}


class DevError(Exception):
    pass


jptz = tz.gettz("Asia/Tokyo")
testb = False


def is_owner_with_response():
    @asyncio.coroutine
    def predicate(ctx):
        if not (yield from ctx.bot.is_owner(ctx.author)):
            return False
        return True

    return commands.check(predicate)
