import discord
from discord.ext import commands
from botmodules import *
import Dconst
from Utilities import guildjsonutilites, rethinkdbaccessorv2
from botsecretstuff2.botsecrets import asuna
bot = commands.Bot(command_prefix="!",
                   self_bot=False, pm_help=True, case_insensitive=True)


@bot.event
async def on_message(message: discord.Message):
    if str(message.author.id) in Dconst.globalbanlist:
        return
    if isinstance(message.channel, discord.DMChannel):
        return await bot.process_commands(message)
    elif isinstance(message.channel, discord.TextChannel):
        GuildJsonInstance = guildjsonutilites.get_instance(message.guild)
        if GuildJsonInstance.get_config("emoji"):
            filteredemojis = filteremoji(message.content)
            if len(filteredemojis) > 0:
                emojimessage = ""
                for emoji in Dconst.elist:
                    if str(emoji[1]) == str(emoji):
                        message += f"{emoji[0]} "
                await message.channel.send(emojimessage[0])
    # Exp.
    await bot.process_commands(message)
    ctx = await bot.get_context(message)
    if not ctx.valid and not ctx.author.bot and rethinkdbaccessorv2.connected and ctx.guild is not None:
        await reg.reg.check(message, bot)


comm = [cashconvert.Currency(bot), emotes.emotes(bot),
        miscellaneouscommands.MiscellaneousCommands(bot), MusicModuleDE.music(bot), quotes.quoteshook(bot),
        reg.reg(bot), search.search(bot),
        Core.Coreo(bot), Core.Moderation(bot), users.users(bot), weebsh.weebsh(bot)]

for i in comm:
    print(f"added {i.__str__()}")
    bot.add_cog(i)

bot.run(asuna)
